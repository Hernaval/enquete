-- risque_debiteur
-- Fichiers en entr�e : /mgrxch/div/bnk/Chapitre_impayes.txt
--                      /mgrxch/div/bnk/cha_solde_debiteur.prn
--			/mgrxch/div/bnk/Code_BCM.txt
-- Fichiers en sortie : /mgrxch/bnk/unl/risque_debiteur.txt
--						/mgrxch/bnk/tmp/risque_debiteur.txt

--12-07-2017: Zoelisoa
--plus de fichiers en entr�e

--06-09-2017: Zoelisoa
--modification sur la r�cup�ration du num�ro pi�ce d identit�

--12-07-2018 : Zoelisoa
--redressement CDR
delete from mgr_risque01_002 where 1=1;
commit;
!/applis/mgrp/bnk/bin/MYLOAD mgr_risque01_002 /mgrxch/div/bnk/code_fiche.txt
INSERT INTO mgr_risque04_003
SELECT a.age,a.dev,a.ncp, a.cha,a.sde   FROM bksld a WHERE a.sde < 0  AND dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY') and (a.cha in (
'2017100000','2030000000','2030000001','2030000002','2030000003','2030000004','2030000005','2030000006','2030000007','2030000013','2030000014','2030000015','2030000016','2030000017',
'2030000018','2030000019','2030000020','2030000021','2030000022','2030000023','2030000024','2030000025','2030000026','2030000027','2030000028','2030000029','2030000030','2030000100',
'2030000101','2030000102','2030000103','2030000104','2030000105','2030000106','2030000107','2030000108','2030000109','2030000110','2030000111','2030000112','2030000113','2030000114',
'2030000124','2030000125','2030000126','2030000203','2030000204','2030000213','2030000214','2030000215','2030000224','2030000314','2030000315','2032000000','2032000001','2032100000',
'2032100001',
'2026000009',
'2070000100',
'2070000101',
'2070000102',
'2070000103',
'2070000104',
'2070000105',
'2070000106',
'2070000120',
'2070000121',
'2070000122',
'2070000123',
'2070000124',
'2070000125',
'2070000126',
'2070000127',
'2070000141',
'2070000142',
'2070000143',
'2070000144',
'2070000145',
'2070000146',
'2070000147',
'2070000148',
'2070000149',
'2070000150',
'2070000151',
'2070000152',
'2070000153',
'2070000154',
'2070000155',
'2070000156',
'2070000157',
'2070000158',
'2070000159',
'2070000160',
'2070000161',
'2070000162',
'2070000163',
'2070000164',
'2070000165',
'2070000200',
'2070000201',
'2070000202',
'2070000203',
'2070000204',
'2070000205',
'2070000206',
'2070000207',
'2070000208',
'2070000209',
'2070000210',
'2070000211',
'2070000212',
'2070000213',
'2070000214',
'2070000215',
'2070000216',
'2070000217',
'2070000218',
'2070000219',
'2070000220',
'2070000221',
'2070000222',
'2070000231',
'2070000232',
'2070000233',
'2070000234',
'2070000235',
'2070000236',
'2070000237',
'2070000238',
'2070000239',
'2070000240',
'2070000241',
'2070000242',
'2070000243',
'2070000244',
'2070000245',
'2070000246',
'2070000247',
'2070000248',
'2070000249',
'2070000250',
'2070000251',
'2070000252',
'2070000253',
'2070000254',
'2070000255',
'2070000256',
'2070000257',
'2070000258',
'2070000259',
'2070000260',
'2070000261',
'2070000262',
'2070000263',
'2070000264',
'2070000265',
'2070000304',
'2070000305',
'2070000348',
'2070001163',
'2070001200',
'2070001210',
'2070001220',
'2070002200',
'2071000100',
'2071000101',
'2071000102',
'2071000103',
'2071000104',
'2071000105',
'2071000106',
'2071000120',
'2071000121',
'2071000122',
'2071000123',
'2071000124',
'2071000127',
'2071000141',
'2071000142',
'2071000143',
'2071000144',
'2071000145',
'2071000146',
'2071000147',
'2071000148',
'2071000149',
'2071000150',
'2071000151',
'2071000152',
'2071000153',
'2071000154',
'2071000155',
'2071000156',
'2071000157',
'2071000158',
'2071000159',
'2071000160',
'2071000161',
'2071000162',
'2071000165',
'2071000166',
'2071000213',
'2071000214',
'2071000215',
'2071000216',
'2071000217',
'2071000218',
'2071000219',
'2071000220',
'2071000221',
'2071000222',
'2071000231',
'2071000232',
'2071000233',
'2071000234',
'2071000235',
'2071000236',
'2071000237',
'2071000238',
'2071000239',
'2071000240',
'2071000241',
'2071000242',
'2071000243',
'2071000244',
'2071000245',
'2071000246',
'2071000247',
'2071000248',
'2071000249',
'2071000250',
'2071000251',
'2071000252',
'2071000253',
'2071000254',
'2071000255',
'2071000256',
'2071000257',
'2071000258',
'2071000259',
'2071000260',
'2071000261',
'2071000262',
'2071000263',
'2071000264',
'2071000265',
'2071001163',
'2071001200',
'2071001210',
'2071001220',
'2071002200',
'2072000000',
'2072000001',
'2072000002',
'2072000003',
'2072000004',
'2072000005',
'2072000006',
'2072000007',
'2072000008',
'2072000009',
'2072000010',
'2072000011',
'2072000012',
'2072000013',
'2072000014',
'2072000015',
'2072000016',
'2072000200',
'2072000201',
'2072000202',
'2072000203',
'2072000204',
'2072000205',
'2072000206',
'2072000207',
'2072000208',
'2072000209',
'2072000210',
'2072000211',
'2072000212',
'2072000213',
'2072000214',
'2072000215',
'2072000216',
'2072000217',
'2072000218',
'2072000219',
'2072000220',
'2072100001',
'2072100002',
'2072100003',
'2072100004',
'2072100005',
'2072100006',
'2072100007',
'2072100008',
'2072100009',
'2072100010',
'2072100011',
'2072100012',
'2072100013',
'2072100014',
'2072100015',
'2072100016',
'2074600000',
'2075100000',
'2075100200',
'2075110000',
'2075110001',
'2075210000',
'2075210001',
'2110000000',
'2110000001',
'2110000002',
'2110000003',
'2110000004',
'2110000005',
'2110100001',
'2110100002',
'2110200001',
'2110200002',
'2110300001',
'2110400001',
'2110400002',
'2110400003',
'2110400004',
'2110400005',
'2110400013',
'2110500001',
'2110500002',
'2110500003',
'2110500011',
'2110500013',
'2111000001',
'2111000002',
'2111000003',
'2111000004',
'2111000005',
'2111010000',
'2111010001',
'2111010002',
'2111100001',
'2111100002',
'2111200001',
'2111200011',
'2111300001',
'2111310000',
'2111400001',
'2195600000',
'2610000200',
'2610000201',
'2620000200',
'2620000201',
'2630000000',
'2630000200',
'2630000201',
'2700000000',
'2700000001',
'2700000011',
'2704100030',
'2710000011',
'2711000000',
'2711000010',
'2720000001',
'2721000000',
'2730000000',
'2730000010',
'2731000000',
'2731000010',
'2741000000',
'2741000010',
'2030000018',
'2750100010',
'2700000012',
'2700000013',
'2700000014',
--chapitre F10
'2040000103',
'2040000104',
'2041000003',
'2051000003',
'2740100000',
'2740100010',
'2741100000',
'2741100010',
'2751100000',
'2751100010',
'2750100010',
'2051000015',
'2740200010',
'2740000010',
'2010000003',
'2150200000'));
--- or a.cha like '213%

INSERT INTO mgr_risque04_003
SELECT a.age,a.dev,a.ncp, a.cha,a.sde   FROM bksld a WHERE a.sde < 0  AND dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY') and a.cha in (
'2040000100','2040000101','2040000102','2040000203','2041000000','2041000004','2041000005','2041000006','2041000007','2041000008','2041000009',
'2041000013','2041000014','2041000015','2041000016','2041000017','2041000018','2041000019','2041000060','2041000070','2041000071','2041000080','2041000081','2041000090','2041000091',
'2041000100','2041000101','2041000102','2041000103','2041000104','2041000105','2041000110','2041000111','2041000112','2041000113','2041000114','2041000115','2041000116','2041000120',
'2041000121','2041000122','2041000123','2041000124','2041000125','2041000126','2041000127','2041000128','2041000129','2041000130','2041000131','2041000132','2041000200','2041000205',
'2041000206','2041000207','2041000221','2041000230','2041000260','2042000003','2051000000','2051000002','2051000005','2051000007','2051000008','2051000010','2051000011',
'2051000060','2051000061','2051000062','2051000063','2051000064','2051000065','2051000070','2051000071','2051000072','2051000110','2051000111','2051000120','2051000121','2051000200',
'2640000200','2640000201','2750100000','2750100010','2700000012','2700000013','2700000014','2051000015')
 and not exists(select 1 from bkcptprt b where trim(a.age)=trim(b.age_cpt) and trim(a.ncp)=trim(b.ncp));

INSERT INTO mgr_risque04_007 SELECT a.age,   a.dev,   a.ncp,   a.sde,   dodb,   substr(a.ncp,2,10),   0, to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'),a.cha,   cli,   dodb,
'   ' code_fiche,   '1' qua_cred,   0 sdeCDL,   cpro  FROM mgr_risque04_003 a,bkcom b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp;

--INSERT INTO mgr_risque04_007 SELECT a.age,   a.dev,   a.ncp,   a.sde,   dodb,   substr(a.ncp,2,10),   0,to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'),   
--a.cha,   cli,   dodb,   '   ' code_fiche,   '1' qua_cred,   0 sdeCDL,   cpro  FROM mgr_risque04_003 a,bkcom b 
--WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND cfe='O'  AND dfe > to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY');
--maj nomenclature
UPDATE mgr_risque04_007 SET code_fiche=(SELECT code_fiche  FROM mgr_risque01_002 WHERE mgr_risque01_002.cha=mgr_risque04_007.cha ) 
WHERE cha IN (SELECT cha  FROM mgr_risque01_002);
--maj de ceux qui ont une autorisation de d�couvert
INSERT INTO mgr_risque04_008 ( SELECT age,   dev,   ncp,   MAX(debut) debut  FROM bkautc WHERE sit != 'A' AND eta IN ('VA','VF') 
AND debut <= to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')  AND ech >= to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')   
GROUP BY age,   dev,   ncp );

INSERT INTO mgr_risque04_009 ( SELECT a.age,   a.dev,   a.ncp,   b.debut,   MAX(naut) naut  FROM mgr_risque04_008 a,bkautc b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND sit != 'A'  AND eta IN ('VA','VF') AND a.debut=b.debut  
AND ech >= to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')   GROUP BY a.age,   a.dev,   a.ncp,   b.debut );

INSERT INTO mgr_risque04_010 ( SELECT a.age,   a.dev,   a.ncp,   b.debut,   b.naut,   maut,   ech  FROM mgr_risque04_009 a,bkautc b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND sit != 'A'  AND eta IN ('VA','VF') AND a.debut=b.debut  
AND a.naut=b.naut  AND ech >= to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_007 SET debut=(SELECT debut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
naut=(SELECT naut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
maut=(SELECT maut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
ech=(SELECT ech  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ) 
WHERE age||ncp IN (SELECT age||ncp  FROM mgr_risque04_010);

--montant CDL
INSERT INTO mgr_risque04_011 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque04_007 a,bkcom b 
WHERE a.cli=b.cli AND b.cha like '27%'  AND cfe='N'  );

INSERT INTO mgr_risque04_012 ( SELECT a.*,   b.sde  FROM mgr_risque04_011 a,bksld b WHERE a.age=b.age AND a.dev=b.dev  
AND a.ncp=b.ncp  AND dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_007 SET sdeCDL=(SELECT SUM(sde)  FROM mgr_risque04_012 WHERE mgr_risque04_007.cli=mgr_risque04_012.cli ) 
WHERE cli IN (SELECT cli  FROM mgr_risque04_012);


UPDATE mgr_risque04_007 SET qua_cred=3 WHERE sdeCDL<>0;

--Modif 08/08/2017

update mgr_risque04_007 set cli=(select cli from bkcom where mgr_risque04_007.age=bkcom.age and mgr_risque04_007.dev=bkcom.dev and mgr_risque04_007.ncp=bkcom.ncp) where age||dev||ncp in (select age||dev||ncp from bkcom);

INSERT INTO mgr_risque04_013 ( SELECT a.*,  substr(b.nid,1,17)  FROM mgr_risque04_007 a left join bkcli b on (trim(a.cli)=trim(b.cli)) );
UPDATE mgr_risque04_013 SET id_personne=(SELECT substr(nidf,1,17)  FROM bkcli WHERE trim(mgr_risque04_013.cli)=trim(bkcli.cli) and nidf is not null and length(trim(nidf))>0) WHERE (id_personne='' or id_personne=' ' or id_personne is null);


INSERT INTO mgr_risque04_014 SELECT a.age,   a.dev,   a.ncp,   MAX(annee)  FROM bkdard a,mgr_risque04_013 b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp   GROUP BY a.age,   a.dev,   a.ncp ;

INSERT INTO mgr_risque04_015 SELECT a.age,   a.dev,   a.ncp,   a.annee,   MAX(mois)  FROM mgr_risque04_014 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee   GROUP BY a.age,   a.dev,   a.ncp,   a.annee ;

INSERT INTO mgr_risque04_016 SELECT a.age,   a.dev,   a.ncp,   a.annee,   a.mois,   MAX(datr)  FROM mgr_risque04_015 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee  AND a.mois=b.mois   GROUP BY a.age,   a.dev,   a.ncp,   a.annee,   a.mois ;

INSERT INTO mgr_risque04_017 SELECT b.age,   b.dev,   b.ncp,   b.taux,   b.plaf  FROM mgr_risque04_016 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee  AND a.mois=b.mois  AND a.datr=b.datr;

INSERT INTO mgr_risque04_018 ( SELECT DISTINCT a.age,   a.dev,   a.ncp,   a.naut,   nvl(b.tdb,0) ,   b.plaf,   a.maut,   a.ech  
FROM mgr_risque04_013 a LEFT JOIN mgr_risque04_017 b ON a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  );


INSERT INTO mgr_risque04_019 ( SELECT age,   dev,   ncp,   COUNT(*) nb  FROM mgr_risque04_018  GROUP BY age,   dev,   ncp having count(*)=1 );

INSERT INTO mgr_risque04_020 ( SELECT a.*  FROM mgr_risque04_018 a,mgr_risque04_019 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  );

INSERT INTO mgr_risque04_021 ( SELECT age,   dev,   ncp,   COUNT(*) nb  FROM mgr_risque04_018  GROUP BY age,   dev,   ncp  having count(*) > 1 );

INSERT INTO mgr_risque04_022 ( SELECT *  FROM mgr_risque04_018 WHERE maut >= plaf );

INSERT INTO mgr_risque04_023 ( SELECT a.age,   a.dev,   a.ncp,   a.naut,   MAX(a.tdb) tdb  
FROM mgr_risque04_022 a , mgr_risque04_021 b  WHERE a.age||a.dev||a.ncp = b.age||b.dev||b.ncp  GROUP BY a.age,   a.dev,   a.ncp,   a.naut );

INSERT INTO mgr_risque04_024 ( SELECT a.*,   b.plaf,   b.maut,   b.ech  FROM mgr_risque04_023 a,mgr_risque04_018 b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  AND a.tdb=b.tdb  );

INSERT INTO mgr_risque04_020 SELECT *  FROM mgr_risque04_024;

INSERT INTO mgr_risque04_025 ( SELECT DISTINCT a.age,   a.dev,   a.ncp  
FROM mgr_risque04_018 a LEFT JOIN mgr_risque04_020 b ON a.age||a.dev||a.ncp = b.age||b.dev||b.ncp  WHERE trim(b.age||b.dev||b.ncp) IS NULL );

INSERT INTO mgr_risque04_026 ( SELECT a.age,   a.dev,   a.ncp,   b.naut,   MAX(tdb) tdb  
FROM mgr_risque04_025 a,mgr_risque04_018 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp   GROUP BY a.age,   a.dev,   a.ncp,   b.naut );

INSERT INTO mgr_risque04_020 SELECT a.*,   b.plaf,   b.maut,   b.ech  
FROM mgr_risque04_026 a,mgr_risque04_018 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  AND a.tdb=b.tdb;

INSERT INTO mgr_risque04_027 ( SELECT DISTINCT a.*,   nvl(b.tdb,0)  FROM mgr_risque04_013 a LEFT JOIN mgr_risque04_020 b 
ON a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  );

-- 23-05-2013

INSERT INTO mgr_risque04_031 SELECT a.age,a.dev,a.ncp , MIN(b.dco) dp_remb FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='C' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

INSERT INTO mgr_risque04_032 SELECT a.age,a.dev,a.ncp , MAX(b.dco) dprec_remb FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='C' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

INSERT INTO mgr_risque04_033 SELECT a.age,a.dev,a.ncp , MAX(b.dco) ddbl FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='D' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

-- 23-05-2013

INSERT INTO mgr_risque04_034 SELECT age,   cli,   ncp,   cha,   '11',   '00008',   to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'),   
id_personne,   naut,   code_fiche,   dev,   debut,   maut,   calc_duree(debut,ech),   ' ',   'AU',   dodb,   0,   ech,   sde_eng,   '',   
qua_cred,   '0',   0,   0,   '0',   0,   0,   sdeCDL,   0,   0,   tdb,   'TF',   '0,0000',   ''  FROM mgr_risque04_027 WHERE ech != '01/01/1111';

INSERT INTO mgr_risque04_034 SELECT age,   cli,   ncp,   cha,   '11',   '00008',   to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'),   
id_personne,   naut,   code_fiche,   dev,   debut,   maut,   '0',   ' ',   'AU',   dodb,   0,   ech,   sde_eng,   '',   qua_cred,   '0',   
0,   0,   '0',   0,   0,   sdeCDL,   0,   0,   tdb,   'TF',   '0,0000',   ''  FROM mgr_risque04_027 WHERE ech='01/01/1111';

UPDATE mgr_risque04_034 SET dpec=(SELECT dp_remb  FROM mgr_risque04_031 WHERE mgr_risque04_034.age=mgr_risque04_031.age AND mgr_risque04_034.dev=mgr_risque04_031.dev  AND mgr_risque04_034.ncp=mgr_risque04_031.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_031.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_031);
UPDATE mgr_risque04_034 SET ddm_eng=(SELECT dprec_remb  FROM mgr_risque04_032 WHERE mgr_risque04_034.age=mgr_risque04_032.age AND mgr_risque04_034.dev=mgr_risque04_032.dev  AND mgr_risque04_034.ncp=mgr_risque04_032.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_032.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_032);
UPDATE mgr_risque04_034 SET ddbl=(SELECT ddbl  FROM mgr_risque04_033 WHERE mgr_risque04_034.age=mgr_risque04_033.age AND mgr_risque04_034.dev=mgr_risque04_033.dev  AND mgr_risque04_034.ncp=mgr_risque04_033.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_033.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_033);
UPDATE mgr_risque04_034 SET mdb=sde_eng WHERE 1=1;
UPDATE mgr_risque04_034 SET mdb=mdb*( -1) WHERE mdb < 0;
UPDATE mgr_risque04_034 SET sde_eng=sde_eng*( -1) WHERE 1=1;

INSERT INTO mgr_risque04_035 ( SELECT DISTINCT trim(a.neng),   mntg,   cnat,   b.cli,   teng  
FROM bkeng a,bkgar b WHERE a.ngar=b.eve AND a.cli=b.cli  AND eta IN ('VA','VF') AND a.sit NOT IN ('A','E') );

INSERT INTO mgr_risque04_036 ( SELECT DISTINCT a.*  FROM mgr_risque04_035 a,bknatg b 
WHERE b.cnat IN ('GEC','NBC','NBT','NDC','NDT','') AND a.cnat=b.cnat  );



UPDATE mgr_risque04_034 SET gar='1',mont=(SELECT SUM(mntg)  FROM mgr_risque04_036 WHERE trim(mgr_risque04_034.ndos)=trim(mgr_risque04_036.neng) ) WHERE trim(ndos) IN (SELECT trim(neng)  FROM mgr_risque04_036);

UPDATE mgr_risque04_034 SET nbe='999' WHERE nbe >= 1000;

---Provision CDL
delete mgr_risque04_011 where 1=1;
delete mgr_risque04_012 where 1=1;
INSERT INTO mgr_risque04_011 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque04_007 a,bkcom b 
WHERE a.cli=b.cli AND b.cha like '29%'  AND cfe='N'  );

INSERT INTO mgr_risque04_012 ( SELECT a.*,   b.sde  FROM mgr_risque04_011 a,bksld b WHERE a.age=b.age AND a.dev=b.dev  
AND a.ncp=b.ncp  AND dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_034 SET monocd=(SELECT SUM(sde)  FROM mgr_risque04_012 WHERE mgr_risque04_034.cli=mgr_risque04_012.cli ) 
WHERE cli IN (SELECT cli  FROM mgr_risque04_012);

insert into mgr_risque03_036_1
select CLI,AGE,NCP,CHA,OPE,ETAB,PER,ID_PERSONNE,NDOS,CODE,DEV,OCTROI,MON_OCTROI,substr(trim(to_char(NBE)),1,4),AMOR,UNPER,DDBL,MDB,DPEC,SDE_ENG,DDM_ENG,QUA_CRED,GAR,MONT,MONOCD,ANTI,MON_ANTI,0,
sde_eng,IMPUT,MONCDL,MONAGIO,PRODUIT,TAU_INT,CODE_TAUX,TEG,COMMENTAIRE,'',to_date('01/01/1900','DD/MM/YYYY'),0,0,0,0,'' from mgr_risque04_034;

update mgr_risque03_036_1 set ndos=age||trim(ndos) where trim(ndos) =substr(ncp,2,10);
update mgr_risque03_036_1 set ddbl=(select dodb from bkcom where mgr_risque03_036_1.age=bkcom.age and mgr_risque03_036_1.dev=bkcom.dev and mgr_risque03_036_1.ncp=bkcom.ncp) where ddbl is null and age||dev||ncp in (select age||dev||ncp from bkcom);

update mgr_risque03_036_1 set interne='N' where 1=1;
update mgr_risque03_036_1 set interne='O' where cha like '207%';
update mgr_risque03_036_1 set ddm_eng='' where ddm_eng='01/01/1111';
update mgr_risque03_036_1 set dd_remb='' where dd_remb='01/01/1900';
update mgr_risque03_036_1 set dpec='' where dpec='01/01/1111';
update mgr_risque03_036_1 set gar='O'  where gar='1';
update mgr_risque03_036_1 set gar='N'  where gar='0';

update mgr_risque03_036_1 set per=substr(per,4,2)||'/'||substr(per,9,2) where 1=1;
update mgr_risque03_036_1 set octroi=ddbl where octroi is null;


update mgr_risque03_036_1 set moncdl=0 where vni30=0 and vni60=0 and vni90=0;
update mgr_risque03_036_1 set qua_cred=1 where vni30=0 and vni60=0 and vni90=0 and moncdl=0;
update mgr_risque03_036_1 set qua_cred=2 where vni30>0 or vni60>0 or vni90>0 or moncdl>0;

update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY') where dd_remb in ('01/01/1111','01/01/1900');
update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY') where dd_remb is null;

update mgr_risque03_036_1 set ddm_eng=(select ddc from bkcom where bkcom.age=substr(mgr_risque03_036_1.ndos,1,5) and bkcom.ncp=substr(mgr_risque03_036_1.ndos,6,11) and length(trim(ndos))=16) where (ddm_eng='') or (ddm_eng is null) or (ddm_eng in ('01/01/1111','01/01/1900'));
update mgr_risque03_036_1 set dpec=octroi where (dpec='') or (dpec is null) or (dpec in ('01/01/1111','01/01/1900'));

update mgr_risque03_036_1 a set octroi=(select ddd from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(a.dev,1,3)=b.dev)
where exists (select 1 from bkcom b where b.age||substr(b.ncp,2,10)=substr(a.ndos,1,15) and substr(a.dev,1,3)=b.dev) and octroi is null;
update mgr_risque03_036_1 a set octroi=(select dodb2 from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(a.dev,1,3)=b.dev)
where exists (select 1 from bkcom b where b.age||substr(b.ncp,2,10)=substr(a.ndos,1,15) and substr(a.dev,1,3)=b.dev) and octroi is null;
update mgr_risque03_036_1 set ddbl=octroi where ddbl is null;
update mgr_risque03_036_1 set dpec=octroi where (dpec='') or (dpec is null) or (dpec in ('01/01/1111','01/01/1900'));
update mgr_risque03_036_1 set dpec=octroi where dpec is null;


--Montant non sign�
update mgr_risque03_036_1 set sde_eng=abs(sde_eng) where sde_eng<0;
----------F06B DEVISES
update mgr_risque03_036_1 set encours=sde_eng*(select tind from bktau where bktau.dev=mgr_risque03_036_1.dev  and dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')) where dev in (select dev from bktau where dco=to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'));
UPDATE mgr_risque03_036_1 SET dev=(SELECT substr(lib2,1,3)  FROM bknom WHERE ctab='005' AND mgr_risque03_036_1.dev=bknom.cacc ) WHERE dev IN (SELECT cacc  FROM bknom WHERE ctab='005' );
UPDATE mgr_risque03_036_1 SET dev='Ar' WHERE dev='MGA';
----------FIN F06B DEVISES

update mgr_risque03_036_1 set encours=abs(encours) where encours<0;
update mgr_risque03_036_1 set sde_eng=abs(sde_eng) where sde_eng<0;
update mgr_risque03_036_1 set moncdl=abs(moncdl) where moncdl<0;
update mgr_risque03_036_1 set moncdl=mdb where mdb<moncdl;
update mgr_risque03_036_1 set qua_cred=1 where moncdl =0;
update mgr_risque03_036_1 set qua_cred=3 where moncdl <>0;
update mgr_risque03_036_1 set encours=sde_eng where dev='Ar';
update mgr_risque03_036_1 set sde_eng=null where dev='Ar';
update mgr_risque03_036_1 set mon_octroi=mdb where mon_octroi=0;

--MAJ id_personne
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,' ','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,'.','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,',','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,'/','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=substr(id_personne,1,12) WHERE id_personne like '%DUP%';


---MODIF 27/06/2019
--------------
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where 
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('01','03','05','07','08','10','12');
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'31' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');
update mgr_risque03_036_1 set ech_per='30'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)='31'  and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');

update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='29'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='28'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');



update mgr_risque03_036_1 set moncdl=encours where cha like '27%';
update mgr_risque03_036_1 set encours=0 where cha like '27%';
update mgr_risque03_036_1 set ddm_eng=ddbl where cha like '27%';
update mgr_risque03_036_1 set ddm_eng=octroi where ech_per - ddm_eng<90  and cha like '27%';

update mgr_risque03_036_1 set ddm_eng= ech_per where ddm_eng is null and cha not like ('27%');

update mgr_risque03_036_1 set encours=mdb where mdb-encours<0 and mdb-encours>-1;
update mgr_risque03_036_1 set moncdl= encours,encours=0 where ech_per - ddm_eng>92 and cha not like ('27%') and code='F10C';
----vni30
update mgr_risque03_036_1 set vni30= encours,vni60=0,vni90=0 where ech_per - ddm_eng>0 and ech_per - ddm_eng<=31 and month(ddm_eng)!=month(ech_per) and cha not like ('27%') and code='F10C';
----vni60
update mgr_risque03_036_1 set vni60= encours,vni30=0,vni90=0 where ech_per - ddm_eng>31 and ech_per - ddm_eng<=62 and cha not like ('27%') and code='F10C';
----vni90
update mgr_risque03_036_1 set vni90= encours,vni30=0,vni60=0 where ech_per - ddm_eng>62 and ech_per - ddm_eng<=90 and cha not like ('27%') and code='F10C';

update mgr_risque03_036_1 set qua_cred=1 where moncdl =0 and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=2 where (vni30!=0 or vni60!=0 or vni90!=0)  and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=3,vni30=0 ,vni60=0 , vni90=0  where moncdl <>0 and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=3,vni30=0 ,vni60=0 , vni90=0  where  cha  like ('27%');



update mgr_risque03_036_1 set commentaire='001' where dev='Ar';
update mgr_risque03_036_1 set commentaire='091' where dev='EUR';
update mgr_risque03_036_1 set commentaire='030' where dev='USD';
update mgr_risque03_036_1 set commentaire='069' where dev='GBP';
update mgr_risque03_036_1 set commentaire='074' where dev='CHF';
update mgr_risque03_036_1 set commentaire='103' where dev='CNY';
update mgr_risque03_036_1 set commentaire='049' where dev='ZAR';
update mgr_risque03_036_1 set commentaire='046' where dev='JPY';


update mgr_risque03_036_1 a set octroi=(select ddd from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
update mgr_risque03_036_1 a set octroi=(select dodb2 from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
--select count(*) from mgr_risque03_03_3 where octroi is null;
update mgr_risque03_036_1 set ddbl=octroi where ddbl is null;
update mgr_risque03_036_1 set dpec=octroi where dpec is null;

update mgr_risque03_036_1 set code='F06B' where code=' ' or code='';


SET feedback off
SET pagesize 0
SET linesize 20000
SET trimspool on
SET colsep | 
SET echo off
SET numformat  9999999999999999990D9999
SPOOL /mgrxch/bnk/tmp/F10F6_a_charger_local.txt
select * from mgr_risque03_036_1 where code='F10C';
SPOOL OFF;

--chargement de F10 trait� en F6

SPOOL /mgrxch/bnk/tmp/F10F6_sans_cdl.txt
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round(encours,2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is not null where a.moncdl=0 and a.dev<>'Ar' and a.code='F10C'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours,2)),
to_char(round(encours,2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is not null where a.moncdl=0 and a.dev='Ar'  and a.code='F10C'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round(encours,2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is null where a.moncdl=0 and a.dev<>'Ar' and a.code='F10C'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours,2)),
to_char(round(encours,2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is null where a.moncdl=0 and a.dev='Ar'  and a.code='F10C';
SPOOL OFF;
-----------------------------------------------------------------------------------------------------------

SPOOL /mgrxch/bnk/tmp/F10F6_avec_cdl.txt
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(a.ddbl,'DD/MM/YYYY'),to_char(a.ddbl,'DD/MM/YYYY'),to_char(a.dpec,'DD/MM/YYYY'),
to_char(a.ddm_eng,'DD/MM/YYYY'),to_char(a.dd_remb,'DD/MM/YYYY'),day(a.dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),a.unper,
a.dev,a.interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round(a.encours,2)),to_char(round((a.sde_eng),2)),
to_char(round(a.vni30,2)),to_char(round(a.vni60,2)),to_char(round(a.vni90,2)),to_char(round(a.moncdl,2)),to_char(round(a.nb_anti,2)),to_char(round(a.mon_anti,2)),'0','0',
to_char(round(a.monagio,2)),to_char(round(a.produit,2)),a.gar,a.qua_cred,to_char(round(a.tau_int,2)),a.code_taux,to_char(round(a.teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is not null where a.moncdl<>0 and a.dev<>'Ar'  and a.code='F10C'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0','0',
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is not null where a.moncdl<>0 and a.dev='Ar'  and a.code='F10C'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(a.ddbl,'DD/MM/YYYY'),to_char(a.ddbl,'DD/MM/YYYY'),to_char(a.dpec,'DD/MM/YYYY'),
to_char(a.ddm_eng,'DD/MM/YYYY'),to_char(a.dd_remb,'DD/MM/YYYY'),day(a.dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),a.unper,
a.dev,a.interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round(a.encours,2)),to_char(round((a.sde_eng),2)),
to_char(round(a.vni30,2)),to_char(round(a.vni60,2)),to_char(round(a.vni90,2)),to_char(round(a.moncdl,2)),to_char(round(a.nb_anti,2)),to_char(round(a.mon_anti,2)),'0','0',
to_char(round(a.monagio,2)),to_char(round(a.produit,2)),a.gar,a.qua_cred,to_char(round(a.tau_int,2)),a.code_taux,to_char(round(a.teg,2)),'','F',''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is null where a.moncdl<>0 and a.dev<>'Ar'  and a.code='F10C'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0','0',
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is null where a.moncdl<>0 and a.dev='Ar'  and a.code='F10C';
SPOOL OFF;


SPOOL /mgrxch/bnk/tmp/F06B.txt
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'',amor,'{@DN}'||c.dna
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is not null where (a.CODE='F06B' or a.code='' or a.code=' ' or a.code is null) and a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-sde_eng-moncdl,2)),
to_char(round((encours),2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'',amor,'{@DN}'||c.dna
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is not null where (a.CODE='F06B' or a.code='' or a.code=' ' or a.code is null) and a.dev='Ar'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'',amor,''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is null where (a.CODE='F06B' or a.code='' or a.code=' ' or a.code is null) and a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),day(dpec)||'/0'||month(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(mgr_getdco,-1)),'DD/MM/YYYY')),unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-sde_eng-moncdl,2)),
to_char(round((encours),2)),'',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'',amor,''
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is null where (a.CODE='F06B' or a.code='' or a.code=' ' or a.code is null) and a.dev='Ar';
SPOOL OFF;

SPOOL /mgrxch/bnk/tmp/F06B_a_charger_local.txt
select * from mgr_risque03_036_1 where CODE='F06B' or code='' or code=' ' or code is null;
SPOOL OFF;

!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F06B.txt /mgrxch/bnk/unl/F06B.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F06B_a_charger_local.txt /mgrxch/bnk/unl/F06B_a_charger_local.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F10F6_sans_cdl.txt /mgrxch/bnk/unl/F10F6_sans_cdl.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F10F6_avec_cdl.txt /mgrxch/bnk/unl/F10F6_avec_cdl.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F10F6_a_charger_local.txt /mgrxch/bnk/unl/F10F6_a_charger_local.txt
