package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Question1")
public class Question1 implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String users;
	private String direction;
	private String note;
	private String commentaire;
	
	
	public Question1() {
		super();
	}


	public Question1(int id, String users, String direction, String note,String commentaire) {
		super();
		this.id = id;
		this.users = users;
		this.direction = direction;
		this.note = note;
		this.commentaire = commentaire;
	}
	
	


	public Question1(String users, String direction, String note,String commentaire) {
		super();
		this.users = users;
		this.direction = direction;
		this.note = note;
		this.commentaire = commentaire;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUsers() {
		return users;
	}


	public void setUsers(String users) {
		this.users = users;
	}


	public String getDirection() {
		return direction;
	}


	public void setDirection(String direction) {
		this.direction = direction;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getCommentaire() {
		return commentaire;
	}


	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
	
	
	
}
