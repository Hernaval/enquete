package com.javaweb.enquete.dao;

import java.util.ArrayList;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Question1;

public interface Question1Repository  extends JpaRepositoryImplementation<Question1, String>{
	@Query("select q from Question1 q where q.users= :param  ")
	public ArrayList<Question1> findResponseByUser(@Param("param")String param);
	
	@Query(value="select q.direction,avg( convert(INT,q.note) ) from Question1 q group by q.direction  ",nativeQuery=true)
	public ArrayList<String> noteGenerale();
	
	@Query(value="select count(q.note) from Question1 q where convert(INT,q.note) >=:inf  and  convert(INT,q.note) <"
			+ "= :sup   ",nativeQuery=true)
	public ArrayList<String> noteVarince(@Param("inf")String inf,@Param("sup")String sup);
	
	@Query("select distinct q.users from Question1 q where q.users = :param")
	public String findUser(@Param("param")String param);
	
	@Query(value="select count(user)  from question1 where user=:param ",nativeQuery=true)
	public int testUser(@Param("param")String param);
}
