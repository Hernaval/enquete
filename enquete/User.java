package com.javaweb.enquete.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Users")
public class User implements Serializable {
	@Id
	@Column(name="idUser")
	private String idUser;

	@OneToMany(mappedBy ="user")
	private Collection<Noter> services ;
	
	@OneToMany(mappedBy ="user")
	private Collection<Noter> criteres ;
	
	@OneToMany(mappedBy="user")
	private Collection<Question3> applications;

	public User() {
		super();
	}

	public User(String idUser) {
		super();
		this.idUser = idUser;
	
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	
	
	
}


