package com.javaweb.enquete.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Noter;
import com.javaweb.enquete.entities.User;

public interface TestRepository extends JpaRepositoryImplementation<Noter, String> {
	
		@Query("select n from Noter n where n.user.idUser = :param and n.service.direction = :dir  ")
		public ArrayList<Noter>findResponseByUser(@Param("param")String param,@Param("dir")String dir);
		
		
		@Query("select n from Noter n where n.user.idUser = :param  ")
		public ArrayList<Noter>findResponseByUser(@Param("param")String param);
		
		@Query(value="select n.service_id_service, "
				+ "avg(convert(INT,n.note)) from Noter n "
				+ "where n.critere_id_critere != 'q3' "
				+ "group by n.service_id_service ",
				nativeQuery=true)
		public ArrayList<String> noteGenerale();
		
		@Query(value="select distinct n.user_id_user from Noter n where n.user_id_user = :param",nativeQuery=true)
		public String findUser(@Param("param")String param);
		
		
		@Query(value="select count(user_id_user)  from noter where user_id_user=:param ",nativeQuery=true)
		public int testUser(@Param("param")String param);
		
		@Modifying
		@Transactional
		@Query(value="delete from noter where service_id_service='sev30'",nativeQuery=true)
		public void del30();
		
}
