package com.javaweb.enquete.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Question;

public interface QuestionListeRepository extends JpaRepositoryImplementation<Question, String> {
	
	@Query("select q.textQuestion from Question q where q.idQuestion = :param")
	public String findQuestion(@Param("param")String param);
	

}
