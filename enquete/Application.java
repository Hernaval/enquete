package com.javaweb.enquete.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Application")
public class Application implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idApp")
	private int idApp;
	private String nomApp;
	private String catApp;
	private String creator;
	
	@OneToMany(mappedBy="application")
	private Collection<Question3> users;
	
	
	
	public Application() {
		super();
	}



	public Application(String nomApp, String catApp, String creator) {
		super();
		this.nomApp = nomApp;
		this.catApp = catApp;
		this.creator = creator;
	}



	public Application(String nomApp, String catApp) {
		super();
		this.nomApp = nomApp;
		this.catApp = catApp;
	}

	

	public Application(int idApp, String nomApp, String catApp, String creator) {
		super();
		this.idApp = idApp;
		this.nomApp = nomApp;
		this.catApp = catApp;
		this.creator = creator;
		
	}



	public Application(int idApp, String nomApp, String catApp) {
		super();
		this.idApp = idApp;
		this.nomApp = nomApp;
		this.catApp = catApp;
	}

	
	


	public int getIdApp() {
		return idApp;
	}



	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}



	public String getNomApp() {
		return nomApp;
	}



	public void setNomApp(String nomApp) {
		this.nomApp = nomApp;
	}



	public String getCatApp() {
		return catApp;
	}



	public void setCatApp(String catApp) {
		this.catApp = catApp;
	}



	public String getCreator() {
		return creator;
	}



	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	
	
	
	
}
