package com.javaweb.enquete.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Question4;

public interface Question4Repository extends JpaRepositoryImplementation<Question4, String> {
	@Query("select q from Question4 q where q.users = :param  ")
	public ArrayList<Question4> findResponseByUser(@Param("param")String param);
	
	@Query("select q.direction, count(q.direction) from Question4 q where q.amelioration ='' group by q.direction")
	public ArrayList<String> nonConcerne();
	
	 @Query(value="select distinct q.users from Question4 q where q.users = :param",nativeQuery=true)
	    public String findUser(@Param("param")String param);
	 
	 @Query(value="select count(users)  from question4 where users=:param ",nativeQuery=true)
		public int testUser(@Param("param")String param);
}	
