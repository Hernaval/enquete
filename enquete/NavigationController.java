package com.javaweb.enquete.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javaweb.enquete.dao.ApplicationRepository;
import com.javaweb.enquete.dao.Question1Repository;
import com.javaweb.enquete.dao.Question3Repository;
import com.javaweb.enquete.dao.Question4Repository;
import com.javaweb.enquete.dao.Question5Repository;
import com.javaweb.enquete.dao.Question6Repository;
import com.javaweb.enquete.dao.QuestionListeRepository;
import com.javaweb.enquete.dao.ServiceRepository;
import com.javaweb.enquete.dao.TestRepository;
import com.javaweb.enquete.dao.UserRepository;
import com.javaweb.enquete.entities.Application;
import com.javaweb.enquete.entities.Critere;
import com.javaweb.enquete.entities.Noter;
import com.javaweb.enquete.entities.NoterQuestion1;
import com.javaweb.enquete.entities.Question;
import com.javaweb.enquete.entities.Question4;
import com.javaweb.enquete.entities.Service;
import com.javaweb.enquete.entities.User;

@Controller
public class NavigationController {
	@Autowired
	private TestRepository testRepo;
	@Autowired
	private QuestionListeRepository qList;
	
	@RequestMapping(value="/")
	public String acceuil(){
		
		return "index";
	}

	@RequestMapping(value="/export")
	public String excel(){
		
		return "excelExport";
	
	}

	@RequestMapping(value = "/question/2")
	public String index(Model model) {
		String question = qList.findQuestion("q2");
		model.addAttribute("question", question);
		return "question2/question2";
	}
	
	@RequestMapping(value = "/question/2IE")
	public String index2(Model model) {
		String question = qList.findQuestion("q2");
		model.addAttribute("question", question);
		return "question2/question2IE";
	}
	
	
	
	@RequestMapping(value = "/question/1")
	public String question1(Model model) {
		String question = qList.findQuestion("q1");
		model.addAttribute("question", question);
		return "question1/question1";
	}
	
	@RequestMapping(value = "/question/3")
	public String question3(Model model) {
		String question = qList.findQuestion("q3");
		model.addAttribute("question", question);
		return "question3/question3";
	}
	
	@RequestMapping(value = "/question/4")
	public String question4(Model model) {
		String question = qList.findQuestion("q4");
		model.addAttribute("question", question);
		return "question4/question4";
	}
	
	@RequestMapping(value = "/question/5")
	public String question5(Model model) {
		String question = qList.findQuestion("q5");
		model.addAttribute("question", question);
		return "question5/question5";
	}

	@RequestMapping(value = "/question/6")
	public String question6(Model model) {
		String question = qList.findQuestion("q6");
		model.addAttribute("question", question);
		return "question6/question6";
	}
	
	@RequestMapping(value="/users")
	public String liste(){
		
		return "listes/users";
		
	}
	
	@RequestMapping(value="/termine")
	public String finish(){
		
		return "finish";
		
	}
	@RequestMapping(value="/stats")
	public String stat(){
		
		return "statistiques/statistique";
		
	}
	
	
	@RequestMapping(value="/resolve")
	public String res(){
		
		return "resolve";
		
	}
	@Autowired
	private ServiceRepository sevRepo;
	@Autowired
	private Question5Repository q5Repo;
	@Autowired
	private Question3Repository q3Repo;
	@Autowired
	private Question4Repository q4Repo;

	@Autowired
	private Question1Repository q1Repo;
	
	@Autowired
	private Question6Repository q6Repo;
	
	private QuestionListeRepository qListe;
	@Autowired
	private ApplicationRepository appRepo;
	@Autowired
	private UserRepository userRepo;
	
	
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public @ResponseBody String del(){
		
		testRepo.deleteAll();
		
		/*int idapp =1;
		String nom = "JIRA SOAPRET";
		String catt = "vitale";
		String creator= "all";
		Application app = new Application(idapp,nom,catt,creator);
		appRepo.save(app);
		
		
		
		
		
		String id = "sev14";
		String dir  ="org";
		String val ="Conduite de projets" ;
		
		Service q = new Service(id,val,dir);
		sevRepo.save(q);
		
		
		String id1 = "sev08";
		String dir1  ="inf";
		String val1 ="Accueil au téléphone et disponibilité des interlocuteurs";
		
		Service q1 = new Service(id1,val1,dir1);
		sevRepo.save(q1);
		
		String id2 = "sev18";
		String dir2  ="org";
		String val2 ="Accueil au téléphone et disponibilité des interlocuteurs";
		
		Service q2 = new Service(id2,val2,dir2);
		sevRepo.save(q2);
		
		String id3 = "sev26";
		String dir3  ="log";
		String val3 ="Accueil au téléphone et disponibilité des interlocuteurs";
		
		Service q3 = new Service(id3,val3,dir3);
		sevRepo.save(q3);
		
		
		String id4 = "sev01";
		String dir4  ="inf";
		String val4 ="Maintenance des matériels  bureautiques (Pc,...)";
		
		Service q4 = new Service(id4,val4,dir4);
		sevRepo.save(q4);
	
		String id5 = "sev12";
		String dir5  ="org";
		String val5 ="Résolution des incidents (accès bloqué, outil non fonctionnel)";
		
		Service q5 = new Service(id5,val5,dir5);
		sevRepo.save(q5);
		
		
		
		String id6 = "sev16";
		String dir6  ="org";
		String val6 ="Gestion des habilitations (Amplitude, Cool,...)";
		
		Service q6 = new Service(id6,val6,dir6);
		sevRepo.save(q6);
		
		
		String id7 = "sev19";
		String dir7  ="log";
		String val7 ="Maintenance matériels (groupe électrogène, etc.)";
		
		Service q7 = new Service(id7,val7,dir7);
		sevRepo.save(q7);
		
		String id8 = "sev20";
		String dir8  ="log";
		String val8 ="Maintenance de mobiliers (chaises, tables ,etc.)";
		Service q8 = new Service(id8,val8,dir8);
		sevRepo.save(q8);
		
		String id18 = "sev22";
		String dir18  ="log";
		String val18 ="Maintenance d'électricité ";
		
		Service q18 = new Service(id18,val18,dir18);
		sevRepo.save(q18);
		
		String id19 = "sev21";
		String dir19  ="log";
		String val19 ="Maintenance plomberie ";
		
		Service q19 = new Service(id19,val19,dir19);
		sevRepo.save(q19);
		
		String id9 = "sev02";
		String dir9  ="inf";
		String val9 ="Maintenance des applicatifs internes";
		
		Service q9 = new Service(id9,val9,dir9);
		sevRepo.save(q9);
		
		
		String id10 = "sev27";
		String dir10  ="log";
		String val10 ="Mise à disposition des fournitures et consommables informatiques";
		
		Service q10 = new Service(id10,val10,dir10);
		sevRepo.save(q10);
		
		String id11 = "sev28";
		String dir11  ="log";
		String val11 ="Mise à disposition des fournitures et consommables administratives/de bureau";
		
		Service q11 = new Service(id11,val11,dir11);
		sevRepo.save(q11);
		
		
		
		

		String id15 = "sev03";
		String dir15  ="inf";
		String val15 ="Gestion des habilitations (session, mail)";
		
		Service q15 = new Service(id15,val15,dir15);
		sevRepo.save(q15);
		
		
	
		String id13 = "sev29";
		String dir13  ="log";
		String val13 ="Mise à disposition des fournitures et consommables éléctriques";
		
		Service q13 = new Service(id13,val13,dir13);
		sevRepo.save(q13);
		 
		
	
		String id14 = "sev13";
		String dir14  ="org";
		String val14 ="Conduite de changement dans le cadre du déploiement de nouvel outil/procédure";
		Service q14 = new Service(id14,val14,dir14);
		sevRepo.save(q14);
		
	
		String id16 = "sev07";
		String dir16  ="inf";
		String val16 ="Demande de requêtes ";
		Service q16 = new Service(id16,val16,dir16);
		sevRepo.save(q16);
		
		String id17 = "sev09";
		String dir17  ="org";
		String val17 ="Demande de requêtes ";
		Service q17 = new Service(id17,val17,dir17);
		sevRepo.save(q17);
		*/
			String id20 = "sev24";
		String dir20  ="log";
		String val20 ="Demande de prestataires externes spécialisés ";
		Service q20 = new Service(id20,val20,dir20);
		sevRepo.save(q20);
		
		/*
		String idq = "q1";
		String intitule ="D'une manière générale, sur une échelle de 1 à 10 à combien évaluez-vous la qualité des prestations fournies par chaque Service ? ";
		Question qsdf = new Question(idq,intitule);
		qListe.save(qsdf);
		

		
		Service q14 = new Service(id14,val14,dir14);
		sevRepo.save(q14);
		String idq = "q1";
		String intitule ="D'une manière générale, sur une échelle de 1 à 10 à combien évaluez-vous la qualité des prestations fournies par chaque Service ? ";
		Question qsdf = new Question(idq,intitule);
		qListe.save(qsdf);
		

		String idq1 = "q4";
		String intitule1 ="Selon vous, quels seraient les axes d'amélioration du Service : ";
		Question qer = new Question(idq1,intitule1);
		qListe.save(qer);
	
		String idq2 = "q2";
		String intitule2 ="Sur une échelle de 1 à 10 à combien évaluez-vous votre satisfaction sur les services listés ci-après ?";
		Question qer2 = new Question(idq2,intitule2);
		qListe.save(qer2);
		
		String idq3 = "q3";
		String intitule3 ="Sur un échelle de 1 à 10, combien évaluez-vous votre satisfaction sur la disponibilité des applications et la qualité du réseau informatique ?";
		Question qer3 = new Question(idq3,intitule3);
		qListe.save(qer3); */
		
		return "ok";
		
	}
	

}
