package com.javaweb.enquete.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Critere")
public class Critere implements Serializable {
	
	@Id
	@Column(name="idCritere")
	private String idCritere;
	private String question;
	
	
	@OneToMany(mappedBy ="critere")
	private Collection<Noter> users ;
	
	@OneToMany(mappedBy ="critere")
	private Collection<Noter> services ;
	public Critere() {
		super();
	}



	public Critere(String idCritere, String question,String commentaire) {
		super();
		this.idCritere = idCritere;
		this.question = question;
		
	}



	public String getIdCritere() {
		return idCritere;
	}



	public void setIdCritere(String idCritere) {
		this.idCritere = idCritere;
	}



	public String getQuestion() {
		return question;
	}



	public void setQuestion(String question) {
		this.question = question;
	}

	
}
