package com.javaweb.enquete.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.User;

public interface UserRepository extends JpaRepositoryImplementation<User, String> {
	@Query("select user from User user where user.idUser like :param")
	public ArrayList<User> findByAs(@Param("param")String param);
	
	
}
