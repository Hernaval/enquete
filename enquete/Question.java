package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Question")
public class Question implements Serializable {
	
	@Id
	@Column(name="idQuestion")
	private String idQuestion;
	@Column(name="textQuestion")
	private String textQuestion;
	
	
	public Question() {
		super();
	}
	public Question(String idQuestion, String textQuestion) {
		super();
		this.idQuestion = idQuestion;
		this.textQuestion = textQuestion;
	}
	public String getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(String idQuestion) {
		this.idQuestion = idQuestion;
	}
	public String getTextQuestion() {
		return textQuestion;
	}
	public void setTextQuestion(String textQuestion) {
		this.textQuestion = textQuestion;
	}
	
	
}
