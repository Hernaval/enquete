package com.javaweb.enquete.restController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.StringUtils;

import com.javaweb.enquete.dao.ApplicationRepository;
import com.javaweb.enquete.dao.CritereRepository;
import com.javaweb.enquete.dao.IpInterface;
import com.javaweb.enquete.dao.Question1Repository;
import com.javaweb.enquete.dao.Question3Repository;
import com.javaweb.enquete.dao.Question4Repository;
import com.javaweb.enquete.dao.Question5Repository;
import com.javaweb.enquete.dao.Question6Repository;
import com.javaweb.enquete.dao.ServiceRepository;
import com.javaweb.enquete.dao.TestRepository;
import com.javaweb.enquete.dao.UserRepository;
import com.javaweb.enquete.entities.Application;
import com.javaweb.enquete.entities.Critere;
import com.javaweb.enquete.entities.Noter;
import com.javaweb.enquete.entities.NoterApplication;
import com.javaweb.enquete.entities.NoterQuestion1;
import com.javaweb.enquete.entities.Question1;
import com.javaweb.enquete.entities.Question3;
import com.javaweb.enquete.entities.Question4;
import com.javaweb.enquete.entities.Question5;
import com.javaweb.enquete.entities.Question6;
import com.javaweb.enquete.entities.Service;
import com.javaweb.enquete.entities.User;

@RestController
public class TestRest implements IpInterface {
	
	
	@Autowired
	private TestRepository testRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private ApplicationRepository appRepo;
	@Autowired
	private ServiceRepository  sevRepo;
	@Autowired
	private CritereRepository qRepo;
	@Autowired
	private Question1Repository q1Repo;
	@Autowired
	private Question3Repository q3Repo;
	@Autowired
	private Question4Repository q4Repo;
	@Autowired
	private Question6Repository q6Repo;
	@Autowired
	private Question5Repository q5Repo;
	
	
	@RequestMapping(value="/getCurrentUser",method=RequestMethod.GET)
	public ResponseEntity<String> check(HttpServletRequest request){
		
		String user =  getClientIpAddress(request);
		
		return new ResponseEntity<>(user,HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/addApp")
	public @ResponseBody String addApp(
			@RequestParam("apps[]") String[] nomApp,
			HttpServletRequest request
			){
		String user = getClientIpAddress(request);
		for(String app : nomApp){
			Application application = new Application(app,"autre",user);
			appRepo.save(application);
		}
		return "ok";
	}
	
	
	@RequestMapping(value="/newUser",method=RequestMethod.POST)
		
		public @ResponseBody String insertUser(HttpServletRequest request){
			String ip = getClientIpAddress(request);
			User users = new User(ip);
			userRepo.save(users);
			
			return ip;
		
	}
	public static String getClientIpAddress(HttpServletRequest request) {
	    for (String header : IpInterface.IP_HEADER_CANDIDATES) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	            return ip;
	        }
	    }
	    return request.getRemoteAddr();
	}
	
	public String findClientComputerName(HttpServletRequest request) {
	    String computerName = null;
	    String remoteAddress = request.getRemoteAddr();
	    System.out.println("remoteAddress: " + remoteAddress);
	    try {
	        InetAddress inetAddress = InetAddress.getByName(remoteAddress);
//	        System.out.println("inetAddress: " + inetAddress);
	        computerName = inetAddress.getHostName();
//	        System.out.println("computerName: " + computerName);
	        if (computerName.equalsIgnoreCase("localhost")) {
	            computerName = java.net.InetAddress.getLocalHost().getCanonicalHostName();
	        } 
	    } catch (UnknownHostException e) {
	        
	    }
	    if(StringUtils.trim(computerName).length()>0) computerName = computerName.toUpperCase();
	    //System.out.println("computerName: " + computerName.replace(".BFVSG.SOCGEN", ""));
	    computerName = computerName.replace(".BFVSG.SOCGEN", "");
	    
	    return computerName;
	}


	
	@RequestMapping(value="/get",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Service>> getAllServices(
			@RequestParam("dir")String dir){
		ArrayList<Service> list = (ArrayList<Service>) sevRepo.getAllServicesDir(dir);
		ResponseEntity<ArrayList<Service>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
	}
	
	@RequestMapping(value="/getService",method=RequestMethod.POST)
	public ResponseEntity<ArrayList<Service>> getServices(){
		ArrayList<Service> list = (ArrayList<Service>) sevRepo.findAll();
		ResponseEntity<ArrayList<Service>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
	}
	
	
	@RequestMapping(value="/getApp",method=RequestMethod.POST)
	public ResponseEntity<ArrayList<Application>> getAllApp(){
		
		ArrayList<Application> list = (ArrayList<Application>) appRepo.findAll();
		ResponseEntity<ArrayList<Application>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
	}
	
	@RequestMapping(value="/getApplication",method=RequestMethod.POST)
	public ResponseEntity<ArrayList<Application>> getAllApps(HttpServletRequest request){
		String user = getClientIpAddress(request);
		ArrayList<Application> list = (ArrayList<Application>) appRepo.getAllAppRelativeTo(user);
		ResponseEntity<ArrayList<Application>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
	}
	
	@RequestMapping(value="/getApps",method=RequestMethod.POST)
	public ResponseEntity<ArrayList<Application>> getAppRelativeToUser(HttpServletRequest request){
		String user = getClientIpAddress(request);
		ArrayList<Application> list = (ArrayList<Application>) appRepo.getAppRelativeTo(user);
		ResponseEntity<ArrayList<Application>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
	}
	
	@RequestMapping(value="/countApp",method=RequestMethod.POST)
	public @ResponseBody int countApp(HttpServletRequest request){
		String user = getClientIpAddress(request);
		int count = appRepo.countApp(user);
		
		return count;
	}
	
	
	
	
	
	
	@RequestMapping(value="/question1",method=RequestMethod.POST)
	public @ResponseBody String question1(
			@RequestParam("notes[]") String[] note,
			@RequestParam("comments[]")String[] comment,
			HttpServletRequest request
			
			){
		String [] dir = {"inf","org","log"};
		String users = getClientIpAddress(request);
		for(int i=0;i<note.length;i++){
			Question1 question = new Question1(users,dir[i],note[i],comment[i]);
			q1Repo.save(question);
		}
		
		
		return "ok";
	}
	
	@RequestMapping(value="/jsonTest",method=RequestMethod.POST) //question 2
	public @ResponseBody String test( 
			@RequestParam("notes[]") String[] note,
			@RequestParam("notes2[]")String[] note2,
			@RequestParam("comments[]")String[] comment,
			@RequestParam("params")String dir,
			HttpServletRequest request
			){
		//les services dispo pour l'inf => sev1 à sev8
		String[] servicesInf = {"sev01","sev02","sev03","sev04","sev05","sev06","sev07","sev08"};
		String[] servicesOrg = {"sev09","sev10","sev11","sev12","sev13","sev14","sev15","sev16","sev17","sev18"};
		String[] servicesLog = {"sev19","sev20","sev21","sev22","sev23","sev24","sev25","sev26","sev27","sev28","sev29"};
		//service Inf
		switch(dir){
		case "inf" : 
			
			send(servicesInf,note,note2,comment,request);
			break;
		case "org" :
			send(servicesOrg,note,note2,comment,request);
			break;
		case "log" : 
			send(servicesLog,note,note2,comment,request);
			break;
		}
		
		return "ok";
	}
	
	@RequestMapping(value="/question3",method=RequestMethod.POST)
	public @ResponseBody String question3(
			@RequestParam("notes[]") String[] note,
			@RequestParam("comments[]")String[] comment,
			@RequestParam("apps[]")String[] app,
			HttpServletRequest request
			){
		User users = new User(getClientIpAddress(request));
		
		for(int i=0;i<note.length;i++){
			int idapp = Integer.parseInt(app[i]);
			Application applications = new Application(); applications.setIdApp(idapp);
			String notes = note[i];
			String comments = comment[i];
			NoterApplication noteApp = new NoterApplication(users.getIdUser(),idapp);
			Question3 q3 = new Question3(noteApp,users,applications,notes,comments);
			
			q3Repo.save(q3);
		}
		
		
		return "ok";
	}
	
	@RequestMapping(value="/question4",method=RequestMethod.POST)
	public @ResponseBody String question4(
			@RequestParam("ameliorations[]") String[] amelioration,
			HttpServletRequest request
			){
		String [] dir = {"inf","org","log"};
		String users =getClientIpAddress(request);
		for(int i=0;i<amelioration.length;i++){
			Question4 question = new Question4(users,dir[i],amelioration[i]);
			q4Repo.save(question);
		}
		
		
		return "ok";
	}
	
	@RequestMapping(value="/question5",method=RequestMethod.POST)
	public @ResponseBody String question5(
			@RequestParam("comments[]") String[] commentaires,
			HttpServletRequest request
			){
	
		String users = getClientIpAddress(request);
		String [] dir = {"inf","org","log"};
		
		for(int i=0;i<commentaires.length;i++){
			Question5 question = new Question5(users,dir[i],commentaires[i]);
			q5Repo.save(question);
		}

		return "ok";
	}


	@RequestMapping(value="/question6",method=RequestMethod.POST)
	public @ResponseBody String question6(
			@RequestParam("comments[]")String [] comment,
			HttpServletRequest request
			){
			String[] dir = {"inf","org","log"};
			String users = getClientIpAddress(request);
			
			for(int i=0; i<comment.length;i++){
				Question6 q6 = new Question6(users,dir[i],comment[i]);
				q6Repo.save(q6);
			}
		
		return "ok";
	}


	public  void send(String[]services,String[] note,String[] note2,String[] comment,HttpServletRequest request){
		User user = new User(getClientIpAddress(request));
		
		
		//q1 => qualite de prestation
		for(int i =0;i<services.length;i++) {
			Critere critere = new Critere(); critere.setIdCritere("q1");
			String val = note[i];
			NoterQuestion1 noteQ = new NoterQuestion1(user.getIdUser(),services[i],critere.getIdCritere());
			Service sev = new Service();sev.setIdService(services[i]);
			Noter noter = new Noter(noteQ,user,sev,critere,val);
			
			testRepo.save(noter);
		}
		//q2 => délai de traitement
		for(int i =0;i<services.length;i++) {
			Critere critere = new Critere(); critere.setIdCritere("q2");
			String val = note2[i];
			NoterQuestion1 noteQ = new NoterQuestion1(user.getIdUser(),services[i],critere.getIdCritere());
			Service sev = new Service();sev.setIdService(services[i]);
			Noter noter = new Noter(noteQ,user,sev,critere,val);
			
			testRepo.save(noter);
		}
		
		//q3 commenaire
		for(int i =0;i<services.length;i++) {
			Critere critere = new Critere(); critere.setIdCritere("q3");
			String val = comment[i];
			NoterQuestion1 noteQ = new NoterQuestion1(user.getIdUser(),services[i],critere.getIdCritere());
			Service sev = new Service();sev.setIdService(services[i]);
			Noter noter = new Noter(noteQ,user,sev,critere,val);
			
			testRepo.save(noter);
		}
		
	}

	
	
	
}
