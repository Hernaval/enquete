package com.javaweb.enquete.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Service")
public class Service implements Serializable {
	
	@Id
	@Column(name="idService")
	private String idService;
	private String intitule;
	private String direction;
	
	
	@OneToMany(mappedBy ="service")
	private Collection<Noter> users ;
	
	@OneToMany(mappedBy ="service")
	private Collection<Noter> criteres ;

	public Service() {
		super();
	}


	public Service(String idService, String intitule, String direction) {
		super();
		this.idService = idService;
		this.intitule = intitule;
		this.direction = direction;
		
	}


	public String getIdService() {
		return idService;
	}


	public void setIdService(String idService) {
		this.idService = idService;
	}


	public String getIntitule() {
		return intitule;
	}


	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}


	public String getDirection() {
		return direction;
	}


	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	
	
	
}
