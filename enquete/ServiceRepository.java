package com.javaweb.enquete.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.javaweb.enquete.entities.Noter;
import com.javaweb.enquete.entities.Service;

public interface ServiceRepository extends JpaRepositoryImplementation<Service, String> {
	@Query("select sev from Service sev where sev.direction = :param ")
	public ArrayList<Service> getAllServicesDir(@Param("param")String param);
	
	@Modifying
	@Transactional
	@Query(value="delete from service where id_service='sev30' ",nativeQuery=true)
	public void del30();
	
}
