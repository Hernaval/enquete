package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="Question3")
public class Question3 implements Serializable {
	
	@EmbeddedId
	private NoterApplication id;
	
	@ManyToOne
	@MapsId("userId")
	private User user;
	
	@ManyToOne
	@MapsId("appId")
	private Application application;
	
	@Column(name="note")
	private String note;
	
	@Column(name="commentaire")
	private String commentaire;

	public Question3() {
		super();
	}

	public Question3(NoterApplication id, User user, Application application, String note, String commentaire) {
		super();
		this.id = id;
		this.user = user;
		this.application = application;
		this.note = note;
		this.commentaire = commentaire;
	}



	public NoterApplication getId() {
		return id;
	}



	public void setId(NoterApplication id) {
		this.id = id;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}


	

	



	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Application getApplication() {
		return application;
	}



	public void setApplication(Application application) {
		this.application = application;
	}



	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}



	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	
	
	
	
}
