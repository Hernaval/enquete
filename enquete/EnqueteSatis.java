package com.javaweb.enquete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.javaweb.enquete.entities.Critere;
import com.javaweb.enquete.entities.Service;
import com.javaweb.enquete.entities.User;


@SpringBootApplication

public class EnqueteSatis extends SpringBootServletInitializer {

	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(EnqueteSatis.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(EnqueteSatis.class, args);
		
		
		
		
	}

}
