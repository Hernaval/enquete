package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NoterQuestion1 implements Serializable {
	
	@Column(name="user_id")
	private String userId;
	@Column(name="service_id")
	private String serviceId;
	@Column(name="critere_id")
	private String critereId;
	
	
	public NoterQuestion1() {
		super();
	}


	public String getCritereId() {
		return critereId;
	}


	public void setCritereId(String critereId) {
		this.critereId = critereId;
	}


	public NoterQuestion1(String userId, String serviceId, String critereId) {
		super();
		this.userId = userId;
		this.serviceId = serviceId;
		this.critereId = critereId;
	}


	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}


	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getServiceId() {
		return serviceId;
	}


	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}



	
	
	
	
	
}
