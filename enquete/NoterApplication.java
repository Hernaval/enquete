package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NoterApplication implements Serializable {
	
	@Column(name="user_id")
	private String userId;
	@Column(name="app_id")
	private int appId;
	
	
	public NoterApplication() {
		super();
	}


	public NoterApplication(String userId, int appId) {
		super();
		this.userId = userId;
		this.appId = appId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public int getAppId() {
		return appId;
	}


	public void setAppId(int appId) {
		this.appId = appId;
	}


	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}


	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	
	
	
}	
