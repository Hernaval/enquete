package com.javaweb.enquete.restController;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javaweb.enquete.dao.Question1Repository;
import com.javaweb.enquete.dao.Question3Repository;
import com.javaweb.enquete.dao.Question4Repository;
import com.javaweb.enquete.dao.Question5Repository;
import com.javaweb.enquete.dao.Question6Repository;
import com.javaweb.enquete.dao.TestRepository;
import com.javaweb.enquete.dao.UserRepository;
import com.javaweb.enquete.entities.Question1;

@Controller
public class UserValidation {
	
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private TestRepository testRepo;
	@Autowired
	private Question1Repository q1Repo;
	@Autowired
	private Question3Repository q3Repo;
	@Autowired
	private Question4Repository q4Repo;
	@Autowired
	private Question5Repository q5Repo;
	@Autowired
	private Question6Repository q6Repo;
	
	
	@RequestMapping(value="/getLastQuestion",method=RequestMethod.POST)
	public @ResponseBody String last(HttpServletRequest request){
		String user = TestRest.getClientIpAddress(request);
		int q1 = q1Repo.testUser(user);
		int q2 = testRepo.testUser(user);
		int q3 = q3Repo.testUser(user);
		int q4 = q4Repo.testUser(user);
		int q5 = q5Repo.testUser(user);
		int q6 = q6Repo.testUser(user);
		int [] count = {q1,q2,q3,q4,q5,q6};
		String[] q = {"q1","q2","q3","q4","q5","q6"};
		List<String> finish = new ArrayList<>();
		for(int i=0;i<q.length;i++){
			if(count[i] > 0){
				finish.add(q[i]);
			}
		}
		String max = finish.get(finish.size() - 1);
		return max;
	}
	
	
	
	@RequestMapping(value="/checkRef1",method=RequestMethod.GET)
	public ResponseEntity<String> check(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = q1Repo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/checkRef2",method=RequestMethod.GET)
	public ResponseEntity<String> check2(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = testRepo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/checkRef3",method=RequestMethod.GET)
	public ResponseEntity<String> check3(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = q3Repo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	@RequestMapping(value="/checkRef4",method=RequestMethod.GET)
	public ResponseEntity<String> check4(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = q4Repo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	@RequestMapping(value="/checkRef5",method=RequestMethod.GET)
	public ResponseEntity<String> check5(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = q5Repo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	@RequestMapping(value="/checkRef6",method=RequestMethod.GET)
	public ResponseEntity<String> check6(HttpServletRequest request){
		ArrayList<String> users = new ArrayList<>();
		String user = TestRest.getClientIpAddress(request);
		String ref = q6Repo.findUser(user);
		users.add(ref);
		if(users.contains(user)){
			return new ResponseEntity<>("ok",HttpStatus.OK);
		}else{
			return new ResponseEntity<>("non ok",HttpStatus.OK);
		}
	}
	
	
}
