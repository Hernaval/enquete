package com.javaweb.enquete.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Question5;

public interface Question5Repository extends JpaRepositoryImplementation<Question5, String> {
	@Query("select q from Question5 q where q.users = :param  ")
	public ArrayList<Question5> findResponseByUser(@Param("param")String param);
	
	@Query("select  count(q.commentaire) from Question5 q where q.commentaire ='' ")
	public ArrayList<String> nonConcerne();
	
	@Query(value="select distinct q.users from Question5 q where q.users = :param",nativeQuery=true)
    public String findUser(@Param("param")String param);
	
	
	
	
	 @Query(value="select count(users) from question5 where users=:param ",nativeQuery=true)
		public int testUser(@Param("param")String param);
}
