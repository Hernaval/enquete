package com.javaweb.enquete.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;


import com.javaweb.enquete.entities.Question6;

public interface Question6Repository extends JpaRepositoryImplementation<Question6, String> {
	@Query("select q from Question6 q where q.users = :param  ")
	public ArrayList<Question6> findResponseByUser(@Param("param")String param);
	
	@Query("select  count(q.direction) from Question6 q where q.commentaire ='' ")
	public ArrayList<String> nonConcerne();
	
	@Query(value="select distinct q.users from Question6 q where q.users = :param",nativeQuery=true)
    public String findUser(@Param("param")String param);
	
	 @Query(value="select count(users)  from question6 where users=:param ",nativeQuery=true)
		public int testUser(@Param("param")String param);
}
