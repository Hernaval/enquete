package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

//question 2

@Entity
@Table(name="Noter")
public class Noter implements Serializable {
	
	@EmbeddedId
	private NoterQuestion1 id;
	
	@ManyToOne
	@MapsId("userId")
	private User user;
	
	@ManyToOne
	@MapsId("serviceId")
	private Service service;
	
	@ManyToOne
	@MapsId("critereId")
	private Critere critere;
	
	@Column(name="note")
	private String note;
	
	


	public Noter() {
		super();
	}

	public Noter(NoterQuestion1 id, User user, Service service, Critere critere, String note) {
		super();
		this.id = id;
		this.user = user;
		this.service = service;
		this.critere = critere;
		this.note = note;
	
	}



	public Critere getCritere() {
		return critere;
	}

	public void setCritere(Critere critere) {
		this.critere = critere;
	}

	public NoterQuestion1 getId() {
		return id;
	}



	
	public void setId(NoterQuestion1 id) {
		this.id = id;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Service getService() {
		return service;
	}



	public void setService(Service service) {
		this.service = service;
	}



	public String getNote() {
		return note;
	}



	public void setNote(String note) {
		this.note = note;
	}



	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}



	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	
	
	
}
