package com.javaweb.enquete.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Question6")
public class Question6 implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String users;
	private String direction;
	private String commentaire;
	
	
	
	public Question6() {
		super();
	}



	public Question6(String users, String direction, String commentaire) {
		super();
		this.users = users;
		this.direction = direction;
		this.commentaire = commentaire;
	}



	public Question6(int id, String users, String direction, String commentaire) {
		super();
		this.id = id;
		this.users = users;
		this.direction = direction;
		this.commentaire = commentaire;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUsers() {
		return users;
	}



	public void setUsers(String users) {
		this.users = users;
	}



	public String getDirection() {
		return direction;
	}



	public void setDirection(String direction) {
		this.direction = direction;
	}



	public String getCommentaire() {
		return commentaire;
	}



	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
	
	
	
}
