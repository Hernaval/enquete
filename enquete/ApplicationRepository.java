package com.javaweb.enquete.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Application;


public interface ApplicationRepository extends JpaRepositoryImplementation<Application, String> {
	@Query("select app from Application app where  app.creator =:param ")
	public ArrayList<Application>getAppRelativeTo(@Param("param")String param);
	
	@Query("select app from Application app where app.creator = 'all'  ")
	public ArrayList<Application>getAllAppRelativeTo(@Param("param")String param);
	
	@Transactional
	@Modifying
	@Query("update Application app set app.creator = 'all' where app.catApp='vitale' ")
	public void updateApp();
	
	@Query(value="select top 2 * from application  order by id_app desc",nativeQuery=true)
	public  ArrayList<Application> getLastRow();
	
	@Query(value="select count(id_app) from application where creator =:param ",nativeQuery=true)
	public int countApp(@Param("param")String param);
}
