package com.javaweb.enquete.restController;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.javaweb.enquete.dao.Question1Repository;
import com.javaweb.enquete.dao.Question3Repository;
import com.javaweb.enquete.dao.Question4Repository;
import com.javaweb.enquete.dao.Question5Repository;
import com.javaweb.enquete.dao.Question6Repository;
import com.javaweb.enquete.dao.TestRepository;
import com.javaweb.enquete.dao.UserRepository;
import com.javaweb.enquete.entities.Noter;
import com.javaweb.enquete.entities.Question1;
import com.javaweb.enquete.entities.Question3;
import com.javaweb.enquete.entities.Question4;
import com.javaweb.enquete.entities.Question5;
import com.javaweb.enquete.entities.Question6;
import com.javaweb.enquete.entities.User;

@RestController
public class ListeRest {
	
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private TestRepository testRepo;
	@Autowired
	private Question1Repository q1Repo;
	@Autowired
	private Question3Repository q3Repo;
	@Autowired
	private Question4Repository q4Repo;
	@Autowired
	private Question5Repository q5Repo;
	@Autowired
	private Question6Repository q6Repo;
	
	@RequestMapping(value="/getUsers",method=RequestMethod.POST)
	public  @ResponseBody ResponseEntity<ArrayList<User>> index(
			@RequestParam("as")String as){
		ArrayList<User> list = new ArrayList<>();
		
			if(as.equals("all")){
				list = (ArrayList<User>) userRepo.findAll();
			}else{
				list = (ArrayList<User>) userRepo.findByAs("%"+as+"%");
			}

		ResponseEntity<ArrayList<User>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}

	@RequestMapping(value="/getUserResponse",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Noter>> response(
			@RequestParam("id")String id,
			@RequestParam("dir")String dir)
	{
		ArrayList<Noter> list = (ArrayList<Noter>) testRepo.findResponseByUser(id,dir);
		ResponseEntity<ArrayList<Noter>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	
	@RequestMapping(value="/deux",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Noter>> deux(
			@RequestParam("id")String id)
	{
		ArrayList<Noter> list = (ArrayList<Noter>) testRepo.findResponseByUser(id);
		ResponseEntity<ArrayList<Noter>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	
	@RequestMapping(value="/getUserResponse1",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Question1>> question1(
			@RequestParam("id")String id
			)
	{
		ArrayList<Question1> list = (ArrayList<Question1>) q1Repo.findResponseByUser(id);
		ResponseEntity<ArrayList<Question1>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	

	@RequestMapping(value="/getUserResponse3",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Question3>> response(
			@RequestParam("id")String id
			)
	{
		ArrayList<Question3> list = (ArrayList<Question3>) q3Repo.findResponseByUser(id);
		ResponseEntity<ArrayList<Question3>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	
	@RequestMapping(value="/getUserResponse4",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Question4>> question4(
			@RequestParam("id")String id
			)
	{
		ArrayList<Question4> list = (ArrayList<Question4>) q4Repo.findResponseByUser(id);
		ResponseEntity<ArrayList<Question4>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	
	@RequestMapping(value="/getUserResponse5",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Question5>> question5(
			 @RequestParam("id")String id
			)
	{
		ArrayList<Question5> list = (ArrayList<Question5>) q5Repo.findResponseByUser(id);
		ResponseEntity<ArrayList<Question5>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	@RequestMapping(value="/getUserResponse6",method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<ArrayList<Question6>> question6(
			@RequestParam("id")String id
			)
	{
		ArrayList<Question6> list = (ArrayList<Question6>) q6Repo.findResponseByUser(id);
		ResponseEntity<ArrayList<Question6>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		
		return resp;
		
	}
	
	
	
	
	
}
