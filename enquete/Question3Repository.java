package com.javaweb.enquete.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import com.javaweb.enquete.entities.Question3;

public interface Question3Repository extends JpaRepositoryImplementation<Question3, String> {
    @Query("select n from Question3 n where n.user.idUser = :param   ")
	public ArrayList<Question3>findResponseByUser(@Param("param")String param);
    
    @Query(value="select distinct q.user_id_user from Question3 q where q.user_id_user = :param",nativeQuery=true)
    public String findUser(@Param("param")String param);
    
    @Query(value="select count(user_id_user)  from question3 where user_id_user=:param ",nativeQuery=true)
	public int testUser(@Param("param")String param);
}
