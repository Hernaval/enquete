package com.javaweb.enquete.dao;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.javaweb.enquete.entities.Critere;

public interface CritereRepository extends JpaRepositoryImplementation<Critere, String> {

}
