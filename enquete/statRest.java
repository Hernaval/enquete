package com.javaweb.enquete.restController;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.javaweb.enquete.dao.Question1Repository;
import com.javaweb.enquete.dao.Question3Repository;
import com.javaweb.enquete.dao.Question4Repository;
import com.javaweb.enquete.dao.Question5Repository;
import com.javaweb.enquete.dao.Question6Repository;
import com.javaweb.enquete.dao.TestRepository;
import com.javaweb.enquete.dao.UserRepository;

@RestController
public class statRest {
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private TestRepository testRepo;
	@Autowired
	private Question1Repository q1Repo;
	@Autowired
	private Question3Repository q3Repo;
	@Autowired
	private Question4Repository q4Repo;
	@Autowired
	private Question5Repository q5Repo;
	@Autowired
	private Question6Repository q6Repo;
	
	@RequestMapping(value="/noteGen1",method=RequestMethod.GET,produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> noteGen1(){
		ArrayList<String> list = q1Repo.noteGenerale();
		
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	@RequestMapping(value="/noteVariance1/{inf}/{sup}",
			method=RequestMethod.GET,
			produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> noteVarl(
			@PathVariable("inf")String inf,
			@PathVariable("sup")String sup){
		ArrayList<String> list = q1Repo.noteVarince(inf,sup);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	//question 2
	@RequestMapping(value="/noteGen2",method=RequestMethod.GET,produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> noteGen2(){
		ArrayList<String> list = testRepo.noteGenerale();
		
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	//question 4
	
	@RequestMapping(value="/nonConcerne4",method=RequestMethod.GET,produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> nonConcerne4(){
		ArrayList<String> list = q4Repo.nonConcerne();
		
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	//question5
	@RequestMapping(value="/nonConcerne5",method=RequestMethod.GET,produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> nonConcerne5(){
		ArrayList<String> list = q5Repo.nonConcerne();
		
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/nonConcerne6",method=RequestMethod.GET,produces ={MimeTypeUtils.APPLICATION_JSON_VALUE})
	public ResponseEntity<ArrayList<String>> nonConcerne6(){
		ArrayList<String> list = q6Repo.nonConcerne();
		
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
}
