-- risque_EPC F07
-- 22-02/2017 : Mamy
-- MGRNOM(ctab) : 	163	risque_credit_amort	lib2	cha (mgr_risque01_006)
-- Fichiers en entr�e : /mgrxch/div/bnk/Chapitre_impayes.txt
--						/mgrxch/div/bnk/Code_BCM.txt
-- Fichiers en sortie : /mgrxch/bnk/unl/risque_EPC.txt
--						/mgrxch/bnk/tmp/risque_EPC.txt

--12-07-2017: Zoelisoa
--plus de fichiers en entr�e

--06-09-2017: Zoelisoa
--modification sur la r�cup�ration du num�ro pi�ce d identit�
----------F07
INSERT INTO mgr_risque01_003 ( SELECT DISTINCT cha,   code_fiche  FROM mgr_risque01_002 );
INSERT INTO mgr_risque01_004 SELECT a.cli,   a.age,   a.ncpe,   '',   '',   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   '',   a.neff,   '',   a.devc,   a.dou,   a.mnat,   calc_duree(a.dou,a.dech),   '',   'AU',   a.dou,   a.mnat,   a.dech,   a.mnat,   '01/01/1111',   '1',   '0',   0,   0,   '0',   0,   '0',   0,   0,   0,   a.tint,   'TF',   '0,0000',   ''
from bkefr a  join bksld b on a.age=b.age and a.ncpe=b.ncp and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and b.sde!=0 where
vie!='9' and  teff in('2','3') and 
a.dou <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND a.dech >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and a.ope in('311','312','313','314','315');

---INSERT INTO mgr_risque01_004 SELECT a.cli,   a.age,   a.ncpe,   '',   '',   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   '',   a.neff,   '',   a.devc,   a.dou,   a.mnat,   calc_duree(a.dou,a.dech),   '',   'AU',   a.dou,   a.mnat,   a.dech,   a.mnat,   '01/01/1111',   '1',   '0',   0,   0,   '0',   0,   '0',   0,   0,   0,   a.tint,   'TF',   '0,0000',   '' 
---from bkhisefr a left join bksld b on a.age=b.age and a.ncpe=b.ncp and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and b.sde!=0  where    teff in('2','3') and 
---a.dou <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND a.dech > to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and a.ope in('311','312','313','314','315')
---and (sor!='S' or(sor='S' and dsor>=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')));

INSERT INTO mgr_risque01_004 SELECT a.cli,   a.age,   a.ncpe,   '',   '',   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   '',   a.neff,   '',   a.devc,   a.dou,   a.mnat,   calc_duree(a.dou,a.dech),   '',   'AU',   a.dou,   a.mnat,   a.dech,   a.mnat,   '01/01/1111',   '1',   '0',   0,   0,   '0',   0,   '0',   0,   0,   0,   a.tint,   'TF',   '0,0000',   '' 
from bkhisefr a left join bksld b on a.age=b.age and a.ncpe=b.ncp and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and b.sde!=0  where    teff in('2','3') and 
a.dech> to_char(MGR_DEBUT_MOIS(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  and a.ope in('311','312','313','314','315')
and dpre>to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY');

--INSERT INTO mgr_risque01_004 SELECT a.cli,   a.age,   a.ncpe,   '',   '',   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   '',   a.neff,   '',   a.devc,   a.dou,   a.mnat,   calc_duree(a.dou,a.dech),   '',   'AU',   a.dou,   a.mnat,   a.dech,   a.mnat,   '01/01/1111',   '1',   '0',   0,   0,   '0',   0,   '0',   0,   0,   0,   a.tint,   'TF',   '0,0000',   '' 
--from bkhisefr a left join bksld b on a.age=b.age and a.ncpe=b.ncp and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and b.sde!=0  where    teff in('2','3') and 
--a.dou <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND a.dech >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and a.ope in('311','312','313','314','315')
--and (sor!='S' or(sor='S' and dsor>=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')));

UPDATE mgr_risque01_004 SET chac=(SELECT cha  FROM bkcom WHERE bkcom.age=mgr_risque01_004.age AND bkcom.ncp=mgr_risque01_004.ncpe ),cpro=(SELECT cpro  FROM bkcom WHERE bkcom.age=mgr_risque01_004.age AND bkcom.ncp=mgr_risque01_004.ncpe ) WHERE age||ncpe IN (SELECT age||ncp  FROM bkcom);
INSERT INTO mgr_risque01_005 ( SELECT *  FROM mgr_risque01_004 WHERE age||dev||ncpe  IN (SELECT age||dev||ncp   FROM bkcom WHERE cpro='025' ) );
UPDATE mgr_risque01_004 SET code=(SELECT code_fiche  FROM mgr_risque01_003 WHERE mgr_risque01_003.cha=mgr_risque01_004.chac ) WHERE chac IN (SELECT cha  FROM mgr_risque01_003);

--UPDATE mgr_risque01_004 SET id_personne=(SELECT id_personne  FROM siie_personne WHERE siie_personne.cli=mgr_risque01_004.cli ) WHERE cli IN (SELECT cli  FROM siie_personne);
--Modif 21/08/2017
UPDATE mgr_risque01_004 SET id_personne=(SELECT substr(nid,1,17)  FROM bkcli WHERE mgr_risque01_004.cli=bkcli.cli and nid is not null and length(trim(nid))>0) WHERE cli IN (SELECT cli  FROM bkcli);
UPDATE mgr_risque01_004 SET id_personne=(SELECT substr(nidf,1,17)  FROM bkcli WHERE mgr_risque01_004.cli=bkcli.cli and nidf is not null and length(trim(nidf))>0) WHERE cli IN (SELECT cli  FROM bkcli);

UPDATE mgr_risque01_004 SET dev=(SELECT substr(lib2,1,3)  FROM bknom WHERE ctab='005' AND mgr_risque01_004.dev=bknom.cacc ) WHERE dev IN (SELECT cacc  FROM bknom WHERE ctab='005' );
UPDATE mgr_risque01_004 SET dev='Ar' WHERE dev='MGA';
UPDATE mgr_risque01_004 SET qua_cred='3' WHERE chac IN (SELECT cha  FROM mgr_risque01_001);
INSERT INTO mgr_risque01_006 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque01_004 a,bkcom b WHERE a.cli=b.cli AND 
exists (select 1 from mgrnom where trim(b.cha) = trim(mgrnom.lib2) and mgrnom.ctab='163')
AND sde != 0  );
INSERT INTO mgr_risque01_007 ( SELECT a.*,   sde  FROM mgr_risque01_006 a,bksld b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );
UPDATE mgr_risque01_004 SET moncdl=(SELECT SUM(sde)  FROM mgr_risque01_007 WHERE mgr_risque01_004.cli=mgr_risque01_007.cli ) WHERE cli IN (SELECT cli  FROM mgr_risque01_007);
INSERT INTO mgr_risque01_008 ( SELECT naut  FROM bkautc WHERE age||ncp IN (SELECT age||ncpe  FROM mgr_risque01_004) AND sit != 'A'  AND eta IN ('VA','VF') AND debut <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND ech >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );
INSERT INTO mgr_risque01_009 ( SELECT a.naut,   b.*  FROM mgr_risque01_008 a,bkeng b WHERE trim(a.naut)=trim(b.neng) );
INSERT INTO mgr_risque01_010 ( SELECT DISTINCT a.neng,   mntg,   cnat,   b.cli,   teng  FROM mgr_risque01_009 a,bkgar b WHERE a.ngar=b.eve AND a.cli=b.cli  AND (cnat IN ('GEC','NBC','NBT','NDC','NDT')) AND eta IN ('VA','VF') AND a.sit NOT IN ('A','E') );
UPDATE mgr_risque01_004 SET gar='1',mont=(SELECT SUM(mntg)  FROM mgr_risque01_010 WHERE mgr_risque01_004.ndos=mgr_risque01_010.neng ) WHERE ndos IN (SELECT neng  FROM mgr_risque01_010);
INSERT INTO mgr_risque01_011 ( SELECT a.cli,   SUM(sde) sde  FROM mgr_risque01_004 a,bkcom b WHERE a.cli=b.cli AND b.cha LIKE '207%'   GROUP BY a.cli );
UPDATE mgr_risque01_011 SET sde=sde*( -1) WHERE sde < 0;
UPDATE mgr_risque01_004 SET monvni=(SELECT sde  FROM mgr_risque01_011 WHERE trim(mgr_risque01_004.cli)=trim(mgr_risque01_011.cli) ) WHERE trim(cli) IN (SELECT trim(cli)  FROM mgr_risque01_011);
DELETE FROM mgr_risque01_004 WHERE age||ncpe||chac||ndos
 IN (SELECT age||ncpe||chac||ndos   FROM mgr_risque01_005);
UPDATE mgr_risque01_004 SET sde_eng=sde_eng*( -1) WHERE sde_eng < 0;


SET heading off
SET feedback off
SET pagesize 0
SET linesize 20000
SET trimspool on
SET colsep | 
SET echo off
SET numformat  9999999999999999990D9999

SPOOL /mgrxch/bnk/tmp/F07.txt
select unique '00008' etab,'00008' etab1,substr(a.per,4,2)||'/'||substr(a.per,9,2),a.ndos,a.ndos ndos1,id_personne,'F07',a.octroi,a.ddbl,a.ddbl ddbl1,a.dpec,'',a.dpec dpec1,'',a.unper,a.dev,'N','N',a.mon_octroi,a.mdb,0,a.sde_eng mdpb1,' ',0,0,0,0,0,0,0,0,0,0,'N',a.qua_cred,a.tau_int,
a.code_taux,a.gar,'','','{@DN}'||b.dna
from mgr_risque01_004 a left join bkcli b on trim(a.cli)=trim(b.cli) and dna is not null where a.dev='Ar'
union
select unique '00008' etab,'00008' etab1,substr(a.per,4,2)||'/'||substr(a.per,9,2),a.ndos,a.ndos ndos1,id_personne,'F07',a.octroi,a.ddbl,a.ddbl ddbl1,a.dpec,'',a.dpec dpec1,'',a.unper,a.dev,'N','N',a.mon_octroi,a.mdb,0,a.sde_eng mdpb1,' ',0,0,0,0,0,0,0,0,0,0,'N',a.qua_cred,a.tau_int,
a.code_taux,a.gar,'','',''
from mgr_risque01_004 a left join bkcli b on trim(a.cli)=trim(b.cli) and dna is null where a.dev='Ar';
SPOOL OFF;

SPOOL /mgrxch/bnk/tmp/risque_EPC_F07.txt
select distinct age,ncpe,chac,ope,etab,per,id_personne,ndos,code,dev,octroi, mon_octroi,nbe,amor,unper,ddbl,mdb,dpec,sde_eng, ddm_eng,qua_cred,gar,mont,monocd,anti,mon_anti,monvni, moncdl,monagio,produit,tau_int,code_taux,teg,commentaire  from mgr_risque01_004;
SPOOL OFF;

!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F07.txt /mgrxch/bnk/unl/F07.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/risque_EPC_F07.txt /mgrxch/bnk/unl/risque_EPC_F07.txt

