-- risque_debiteur
-- Fichiers en entr�e : /mgrxch/div/bnk/Chapitre_impayes.txt
--                      /mgrxch/div/bnk/cha_solde_debiteur.prn
--			/mgrxch/div/bnk/Code_BCM.txt
-- Fichiers en sortie : /mgrxch/bnk/unl/risque_debiteur.txt
--						/mgrxch/bnk/tmp/risque_debiteur.txt
--12-07-2017: Zoelisoa
--plus de fichiers en entr�e

--06-09-2017: Zoelisoa
--modification sur la r�cup�ration du num�ro pi�ce d identit�

--12-07-2018 : Zoelisoa
--redressement CDR
delete from mgr_risque01_002 where 1=1;
commit;
!/applis/mgrp/bnk/bin/MYLOAD mgr_risque01_002 /mgrxch/div/bnk/code_fiche.txt
INSERT INTO mgr_risque04_003 
SELECT a.age,a.dev,a.ncp, a.cha,a.sde   FROM bksld a WHERE a.sde < 0  AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and 
(a.cha='2740200010');

INSERT INTO mgr_risque04_007 SELECT a.age,   a.dev,   a.ncp,   a.sde,   dodb,   substr(a.ncp,2,10),   0,to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),a.cha,   cli,   dodb,
'   ' code_fiche,   '1' qua_cred,   0 sdeCDL,   cpro  FROM mgr_risque04_003 a,bkcom b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND cfe='N';

INSERT INTO mgr_risque04_007 SELECT a.age,   a.dev,   a.ncp,   a.sde,   dodb,   substr(a.ncp,2,10),   0,to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   
a.cha,   cli,   dodb,   '   ' code_fiche,   '1' qua_cred,   0 sdeCDL,   cpro  FROM mgr_risque04_003 a,bkcom b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND cfe='O'  AND dfe >to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY');
--maj nomenclature
UPDATE mgr_risque04_007 SET code_fiche=(SELECT code_fiche  FROM mgr_risque01_002 WHERE mgr_risque01_002.cha=mgr_risque04_007.cha ) 
WHERE cha IN (SELECT cha  FROM mgr_risque01_002);
--maj de ceux qui ont une autorisation de d�couvert
INSERT INTO mgr_risque04_008 ( SELECT age,   dev,   ncp,   MAX(debut) debut  FROM bkautc WHERE sit != 'A' AND eta IN ('VA','VF') 
AND debut <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND ech >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   
GROUP BY age,   dev,   ncp );

INSERT INTO mgr_risque04_009 ( SELECT a.age,   a.dev,   a.ncp,   b.debut,   MAX(naut) naut  FROM mgr_risque04_008 a,bkautc b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND sit != 'A'  AND eta IN ('VA','VF') AND a.debut=b.debut  
AND ech >=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   GROUP BY a.age,   a.dev,   a.ncp,   b.debut );

INSERT INTO mgr_risque04_010 ( SELECT a.age,   a.dev,   a.ncp,   b.debut,   b.naut,   maut,   ech  FROM mgr_risque04_009 a,bkautc b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND sit != 'A'  AND eta IN ('VA','VF') AND a.debut=b.debut  
AND a.naut=b.naut  AND ech >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_007 SET debut=(SELECT debut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
naut=(SELECT naut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
maut=(SELECT maut  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ),
ech=(SELECT ech  FROM mgr_risque04_010 WHERE mgr_risque04_010.age=mgr_risque04_007.age AND mgr_risque04_010.ncp=mgr_risque04_007.ncp ) 
WHERE age||ncp IN (SELECT age||ncp  FROM mgr_risque04_010);

--montant CDL
INSERT INTO mgr_risque04_011 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque04_007 a,bkcom b 
WHERE a.cli=b.cli AND b.cha like '27%'  AND cfe='N'  );

INSERT INTO mgr_risque04_012 ( SELECT a.*,   sde  FROM mgr_risque04_011 a,bksld b WHERE a.age=b.age AND a.dev=b.dev  
AND a.ncp=b.ncp  AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_007 SET sdeCDL=(SELECT SUM(sde)  FROM mgr_risque04_012 WHERE mgr_risque04_007.cli=mgr_risque04_012.cli ) 
WHERE cli IN (SELECT cli  FROM mgr_risque04_012);


UPDATE mgr_risque04_007 SET qua_cred=3 WHERE sdeCDL<>0;

--Modif 08/08/2017

update mgr_risque04_007 set cli=(select cli from bkcom where mgr_risque04_007.age=bkcom.age and mgr_risque04_007.dev=bkcom.dev and mgr_risque04_007.ncp=bkcom.ncp) where age||dev||ncp in (select age||dev||ncp from bkcom);

INSERT INTO mgr_risque04_013 ( SELECT a.*,  substr(b.nid,1,17)  FROM mgr_risque04_007 a left join bkcli b on (trim(a.cli)=trim(b.cli)) );
UPDATE mgr_risque04_013 SET id_personne=(SELECT substr(nidf,1,17)  FROM bkcli WHERE trim(mgr_risque04_013.cli)=trim(bkcli.cli) and nidf is not null and length(trim(nidf))>0) WHERE (id_personne='' or id_personne=' ' or id_personne is null);


INSERT INTO mgr_risque04_014 SELECT a.age,   a.dev,   a.ncp,   MAX(annee)  FROM bkdard a,mgr_risque04_013 b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp   GROUP BY a.age,   a.dev,   a.ncp ;

INSERT INTO mgr_risque04_015 SELECT a.age,   a.dev,   a.ncp,   a.annee,   MAX(mois)  FROM mgr_risque04_014 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee   GROUP BY a.age,   a.dev,   a.ncp,   a.annee ;

INSERT INTO mgr_risque04_016 SELECT a.age,   a.dev,   a.ncp,   a.annee,   a.mois,   MAX(datr)  FROM mgr_risque04_015 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee  AND a.mois=b.mois   GROUP BY a.age,   a.dev,   a.ncp,   a.annee,   a.mois ;

INSERT INTO mgr_risque04_017 SELECT b.age,   b.dev,   b.ncp,   b.taux,   b.plaf  FROM mgr_risque04_016 a,bkdard b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.annee=b.annee  AND a.mois=b.mois  AND a.datr=b.datr;

INSERT INTO mgr_risque04_018 ( SELECT DISTINCT a.age,   a.dev,   a.ncp,   a.naut,   nvl(b.tdb,0) ,   b.plaf,   a.maut,   a.ech  
FROM mgr_risque04_013 a LEFT JOIN mgr_risque04_017 b ON a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  );


INSERT INTO mgr_risque04_019 ( SELECT age,   dev,   ncp,   COUNT(*) nb  FROM mgr_risque04_018  GROUP BY age,   dev,   ncp having count(*)=1 );

INSERT INTO mgr_risque04_020 ( SELECT a.*  FROM mgr_risque04_018 a,mgr_risque04_019 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  );

INSERT INTO mgr_risque04_021 ( SELECT age,   dev,   ncp,   COUNT(*) nb  FROM mgr_risque04_018  GROUP BY age,   dev,   ncp  having count(*) > 1 );

INSERT INTO mgr_risque04_022 ( SELECT *  FROM mgr_risque04_018 WHERE maut >= plaf );

INSERT INTO mgr_risque04_023 ( SELECT a.age,   a.dev,   a.ncp,   a.naut,   MAX(a.tdb) tdb  
FROM mgr_risque04_022 a , mgr_risque04_021 b  WHERE a.age||a.dev||a.ncp = b.age||b.dev||b.ncp  GROUP BY a.age,   a.dev,   a.ncp,   a.naut );

INSERT INTO mgr_risque04_024 ( SELECT a.*,   b.plaf,   b.maut,   b.ech  FROM mgr_risque04_023 a,mgr_risque04_018 b 
WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  AND a.tdb=b.tdb  );

INSERT INTO mgr_risque04_020 SELECT *  FROM mgr_risque04_024;

INSERT INTO mgr_risque04_025 ( SELECT DISTINCT a.age,   a.dev,   a.ncp  
FROM mgr_risque04_018 a LEFT JOIN mgr_risque04_020 b ON a.age||a.dev||a.ncp = b.age||b.dev||b.ncp  WHERE trim(b.age||b.dev||b.ncp) IS NULL );

INSERT INTO mgr_risque04_026 ( SELECT a.age,   a.dev,   a.ncp,   b.naut,   MAX(tdb) tdb  
FROM mgr_risque04_025 a,mgr_risque04_018 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp   GROUP BY a.age,   a.dev,   a.ncp,   b.naut );

INSERT INTO mgr_risque04_020 SELECT a.*,   b.plaf,   b.maut,   b.ech  
FROM mgr_risque04_026 a,mgr_risque04_018 b WHERE a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  AND a.tdb=b.tdb;

INSERT INTO mgr_risque04_027 ( SELECT DISTINCT a.*,   nvl(b.tdb,0)  FROM mgr_risque04_013 a LEFT JOIN mgr_risque04_020 b 
ON a.age=b.age AND a.dev=b.dev  AND a.ncp=b.ncp  AND a.naut=b.naut  );

-- 23-05-2013

INSERT INTO mgr_risque04_031 SELECT a.age,a.dev,a.ncp , MIN(b.dco) dp_remb FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='C' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

INSERT INTO mgr_risque04_032 SELECT a.age,a.dev,a.ncp , MAX(b.dco) dprec_remb FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='C' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

INSERT INTO mgr_risque04_033 SELECT a.age,a.dev,a.ncp , MAX(b.dco) ddbl FROM mgr_risque04_013 a , bkhis b 
WHERE a.age = b.age and a.dev = b.dev and a.ncp = b.ncp AND b.sen='D' and b.dco > a.dodb GROUP BY a.age,a.dev,a.ncp ;

-- 23-05-2013

INSERT INTO mgr_risque04_034 SELECT age,   cli,   ncp,   cha,   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   
id_personne,   naut,   code_fiche,   dev,   debut,   maut,   calc_duree(debut,ech),   ' ',   'AU',   dodb,   0,   ech,   sde_eng,   '',   
qua_cred,   '0',   0,   0,   '0',   0,   0,   sdeCDL,   0,   0,   tdb,   'TF',   '0,0000',   ''  FROM mgr_risque04_027 WHERE ech != '01/01/1111';

INSERT INTO mgr_risque04_034 SELECT age,   cli,   ncp,   cha,   '11',   '00008',   to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   
id_personne,   naut,   code_fiche,   dev,   debut,   maut,   '0',   ' ',   'AU',   dodb,   0,   ech,   sde_eng,   '',   qua_cred,   '0',   
0,   0,   '0',   0,   0,   sdeCDL,   0,   0,   tdb,   'TF',   '0,0000',   ''  FROM mgr_risque04_027 WHERE ech='01/01/1111';

UPDATE mgr_risque04_034 SET dpec=(SELECT dp_remb  FROM mgr_risque04_031 WHERE mgr_risque04_034.age=mgr_risque04_031.age AND mgr_risque04_034.dev=mgr_risque04_031.dev  AND mgr_risque04_034.ncp=mgr_risque04_031.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_031.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_031);
UPDATE mgr_risque04_034 SET ddm_eng=(SELECT dprec_remb  FROM mgr_risque04_032 WHERE mgr_risque04_034.age=mgr_risque04_032.age AND mgr_risque04_034.dev=mgr_risque04_032.dev  AND mgr_risque04_034.ncp=mgr_risque04_032.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_032.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_032);
UPDATE mgr_risque04_034 SET ddbl=(SELECT ddbl  FROM mgr_risque04_033 WHERE mgr_risque04_034.age=mgr_risque04_033.age AND mgr_risque04_034.dev=mgr_risque04_033.dev  AND mgr_risque04_034.ncp=mgr_risque04_033.ncp  AND mgr_risque04_034.ndos=substr(mgr_risque04_033.ncp,2,(11)-(2)+1) ) WHERE age||dev||ncp||ndos  IN (SELECT age||dev||ncp||substr(ncp,2,(11)-(2)+1)   FROM mgr_risque04_033);
UPDATE mgr_risque04_034 SET mdb=sde_eng WHERE 1=1;
UPDATE mgr_risque04_034 SET mdb=mdb*( -1) WHERE mdb < 0;
UPDATE mgr_risque04_034 SET sde_eng=sde_eng*( -1) WHERE 1=1;

INSERT INTO mgr_risque04_035 ( SELECT DISTINCT trim(a.neng),   mntg,   cnat,   b.cli,   teng  
FROM bkeng a,bkgar b WHERE a.ngar=b.eve AND a.cli=b.cli  AND eta IN ('VA','VF') AND a.sit NOT IN ('A','E') );

INSERT INTO mgr_risque04_036 ( SELECT DISTINCT a.*  FROM mgr_risque04_035 a,bknatg b 
WHERE b.cnat IN ('GEC','NBC','NBT','NDC','NDT','') AND a.cnat=b.cnat  );



UPDATE mgr_risque04_034 SET gar='1',mont=(SELECT SUM(mntg)  FROM mgr_risque04_036 WHERE mgr_risque04_034.ndos=mgr_risque04_036.neng ) WHERE ndos IN (SELECT neng  FROM mgr_risque04_036);

UPDATE mgr_risque04_034 SET nbe='999' WHERE nbe >= 1000;

---Provision CDL
delete mgr_risque04_011 where 1=1;
delete mgr_risque04_012 where 1=1;
INSERT INTO mgr_risque04_011 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque04_007 a,bkcom b 
WHERE a.cli=b.cli AND b.cha like '29%'  AND cfe='N'  );

INSERT INTO mgr_risque04_012 ( SELECT a.*,   sde  FROM mgr_risque04_011 a,bksld b WHERE a.age=b.age AND a.dev=b.dev  
AND a.ncp=b.ncp  AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );

UPDATE mgr_risque04_034 SET monocd=(SELECT SUM(sde)  FROM mgr_risque04_012 WHERE mgr_risque04_034.cli=mgr_risque04_012.cli ) 
WHERE cli IN (SELECT cli  FROM mgr_risque04_012);

insert into mgr_risque03_036_1
select CLI,AGE,NCP,CHA,OPE,ETAB,PER,ID_PERSONNE,NDOS,CODE,DEV,OCTROI,MON_OCTROI,substr(trim(to_char(NBE)),1,4),AMOR,UNPER,DDBL,MDB,DPEC,SDE_ENG,DDM_ENG,QUA_CRED,GAR,MONT,MONOCD,ANTI,MON_ANTI,0,
sde_eng,IMPUT,MONCDL,MONAGIO,PRODUIT,TAU_INT,CODE_TAUX,TEG,COMMENTAIRE,'',to_date('01/01/1900','DD/MM/YYYY'),0,0,0,0,'' from mgr_risque04_034;

update mgr_risque03_036_1 set ndos=age||trim(ndos) where trim(ndos) =substr(ncp,2,10);
update mgr_risque03_036_1 set interne='N' where 1=1;
update mgr_risque03_036_1 set interne='O' where cha like '207%';
update mgr_risque03_036_1 set ddm_eng='' where ddm_eng='01/01/1111';
update mgr_risque03_036_1 set dd_remb='' where dd_remb='01/01/1900';
update mgr_risque03_036_1 set dpec='' where dpec='01/01/1111';
update mgr_risque03_036_1 set gar='O'  where gar='1';
update mgr_risque03_036_1 set gar='N'  where gar='0';
update mgr_risque03_036_1 set amor='F'  where gar='0';

update mgr_risque03_036_1 set per=substr(per,4,2)||'/'||substr(per,9,2) where 1=1;
update mgr_risque03_036_1 set octroi=ddbl where octroi is null;

update mgr_risque03_036_1 set moncdl=0 where vni30=0 and vni60=0 and vni90=0;
update mgr_risque03_036_1 set qua_cred=1 where vni30=0 and vni60=0 and vni90=0 and moncdl=0;
update mgr_risque03_036_1 set qua_cred=2 where vni30>0 or vni60>0 or vni90>0 or moncdl>0;

--date de dernier remboursement
update mgr_risque03_036_1 set dd_remb=(select to_char(max(ddec)) from bkdosprt,bkcptprt where mgr_risque03_036_1.age=bkdosprt.age and trim(mgr_risque03_036_1.ndos)=bkdosprt.eve and bkdosprt.age=bkcptprt.age and bkdosprt.eve=bkcptprt.eve and bkdosprt.ave=bkcptprt.ave and bkcptprt.nat='004') where age||trim(ndos) in (select age||eve from bkdosprt);
update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') where dd_remb in ('01/01/1111','01/01/1900');
update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') where dd_remb is null;

update mgr_risque03_036_1 set ddm_eng=(select ddc from bkcom where bkcom.age=substr(mgr_risque03_036_1.ndos,1,5) and bkcom.ncp=substr(mgr_risque03_036_1.ndos,6,11) and length(trim(ndos))=16) where (ddm_eng='') or (ddm_eng is null) or (ddm_eng in ('01/01/1111','01/01/1900'));
update mgr_risque03_036_1 set dpec=octroi where (dpec='') or (dpec is null) or (dpec in ('01/01/1111','01/01/1900'));

----------F15_ACA DEVISES
update mgr_risque03_036_1 set encours=sde_eng*(select tind from bktau where bktau.dev=mgr_risque03_036_1.dev  and dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where dev in (select dev from bktau where dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'));
UPDATE mgr_risque03_036_1 SET dev=(SELECT substr(lib2,1,3)  FROM bknom WHERE ctab='005' AND mgr_risque03_036_1.dev=bknom.cacc ) WHERE dev IN (SELECT cacc  FROM bknom WHERE ctab='005' );
UPDATE mgr_risque03_036_1 SET dev='Ar' WHERE dev='MGA';
----------FIN F15_ACA DEVISES
--MAJ id_personne
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,' ','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,'.','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,',','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=replace(id_personne,'/','') WHERE 1=1;
UPDATE mgr_risque03_036_1 SET id_personne=substr(id_personne,1,12) WHERE id_personne like '%DUP%';


---MODIF 27/06/2019
--------------
update mgr_risque03_036_1 set ech_per=day(dpec)||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where 
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('01','03','05','07','08','10','12');
update mgr_risque03_036_1 set ech_per=day(dpec)||'/'||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'31' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');
update mgr_risque03_036_1 set ech_per='30'||'/'||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)='31'  and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');

update mgr_risque03_036_1 set ech_per=day(dpec)||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='29'||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per=day(dpec)||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='28'||decode(length(month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))),1,'0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')),month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');



update mgr_risque03_036_1 set moncdl=encours where cha like '27%';
update mgr_risque03_036_1 set encours=0 where cha like '27%';
update mgr_risque03_036_1 set ddm_eng=ddbl where cha like '27%';
update mgr_risque03_036_1 set ddm_eng=octroi where ech_per - ddm_eng<90  and cha like '27%';

update mgr_risque03_036_1 set ddm_eng= ech_per where ddm_eng is null and cha not like ('27%');

update mgr_risque03_036_1 set encours=mdb where mdb-encours<0 and mdb-encours>-1;
update mgr_risque03_036_1 set moncdl= encours,encours=0 where ech_per - ddm_eng>92 and cha not like ('27%') and code='F11';
----vni30
update mgr_risque03_036_1 set vni30= encours,vni60=0,vni90=0 where ech_per - ddm_eng>0 and ech_per - ddm_eng<=31 and month(ddm_eng)!=month(ech_per) and cha not like ('27%') and code='F11';
----vni60
update mgr_risque03_036_1 set vni60= encours,vni30=0,vni90=0 where ech_per - ddm_eng>31 and ech_per - ddm_eng<=62 and cha not like ('27%') and code='F11';
----vni90
update mgr_risque03_036_1 set vni90= encours,vni30=0,vni60=0 where ech_per - ddm_eng>62 and ech_per - ddm_eng<=90 and cha not like ('27%') and code='F11';

update mgr_risque03_036_1 set qua_cred=1 where moncdl =0 and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=2 where (vni30!=0 or vni60!=0 or vni90!=0)  and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=3,vni30=0 ,vni60=0 , vni90=0  where moncdl <>0 and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=3,vni30=0 ,vni60=0 , vni90=0  where  cha  like ('27%');



update mgr_risque03_036_1 set commentaire='001' where dev='Ar';
update mgr_risque03_036_1 set commentaire='091' where dev='EUR';
update mgr_risque03_036_1 set commentaire='030' where dev='USD';
update mgr_risque03_036_1 set commentaire='069' where dev='GBP';
update mgr_risque03_036_1 set commentaire='074' where dev='CHF';
update mgr_risque03_036_1 set commentaire='103' where dev='CNY';
update mgr_risque03_036_1 set commentaire='049' where dev='ZAR';
update mgr_risque03_036_1 set commentaire='046' where dev='JPY';


update mgr_risque03_036_1 a set octroi=(select ddd from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
update mgr_risque03_036_1 a set octroi=(select dodb2 from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
--select count(*) from mgr_risque03_03_3 where octroi is null;
update mgr_risque03_036_1 set ddbl=octroi where ddbl is null;
update mgr_risque03_036_1 set dpec=octroi where dpec is null;

-----------------

SET heading off
SET feedback off
SET pagesize 0
SET linesize 20000
SET trimspool on
SET colsep | 
SET echo off
SET numformat  9999999999999999990D9999
SPOOL /mgrxch/bnk/tmp/f11_2740200010.txt
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is not null where code='F11' and a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),' ',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is not null where code='F11' and a.dev='Ar'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is null where code='F11' and a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),' ',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is null where code='F11' and a.dev='Ar'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
where code='F11' and a.dev<>'Ar' and not exists(select 1 from bkcli c where trim(a.cli)=trim(c.cli))
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),' ',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a where code='F11' and a.dev='Ar' and not exists(select 1 from bkcli c where trim(a.cli)=trim(c.cli));
SPOOL OFF;
SPOOL /mgrxch/bnk/tmp/F_a_charger_2740200010.txt
select * from bank.mgr_risque03_036_1;
SPOOL OFF;
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/f11_2740200010.txt /mgrxch/bnk/unl/f11_2740200010.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F_a_charger_2740200010.txt /mgrxch/bnk/unl/F_a_charger_2740200010.txt

