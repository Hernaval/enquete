-- risque_credit_amort
-- Fichiers en entr�e : /mgrxch/div/bnk/Chapitre_impayes.txt
--			/mgrxch/div/bnk/Code_BCM.txt
-- Fichiers en sortie : /mgrxch/bnk/unl/risque_credit_amort.txt
--						/mgrxch/bnk/tmp/risque_credit_amort.txt

--12-07-2017: Zoelisoa
--plus de fichiers en entr�e

--06-09-2017: Zoelisoa
--modification sur la r�cup�ration du num�ro pi�ce d identit�

INSERT INTO mgr_risque03_006 ( SELECT age,   eve,   MAX(ave) ave  FROM bkdosprt  where eta IN ('VA','DE') and dou <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  AND
(((ddec >= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) and dtan is null) or
 ((dtan >= to_char(MGR_DEBUT_MOIS(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) and ctr in ('9','5','3','2'))) GROUP BY age,   eve );
--========================================================================================================================
INSERT INTO mgr_risque03_016
select a.age,a.eve,a.ave,a.cli,   a.eta,   a.ctr,   b.dev devprt,   a.octroi,   a.mon,   calc_duree(a.dpec,a.ddec) nbe,   
tya amor,   per_cap unper,   a.ddbl,   mdb,   dpec,   remb,   a.cum_int,   a.tau_int,   ind,   a.teg,b.age,   b.ncp ,   c.cha 
from mgr_risque03_006 d,bkdosprt a,bkcptprt b,bksld c where 
d.age=a.age and d.eve=a.eve and d.ave=a.ave and 
a.age=b.age and a.eve=b.eve and a.ave=b.ave 
and b.age=c.age and b.dev=c.dev and b.ncp=c.ncp and
c.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') and c.sde<>0 and b.nat='004' and a.eta in ('VA','DE') and c.cha in
('2021000000', --202 100 000 0--
'2021000001', --202 100 000 1--
'2021000010', --202 100 001 0--
'2021000011', --202 100 001 1--
'2021000012', --202 100 001 2--
'2021000021', --202 100 002 1--
'2021000022', --202 100 002 2--
'2021100000', 
'2022000001',
'2022000002',
'2022000010',
'2022000011',
'2022000012',
'2022000022',
'2022000023',
'2022000024',
'2022000025',
'2022000026',
'2022000027',
'2022000028',
'2022000029',
'2022000030',
'2022000031',
'2022000032',
'2022100000',
'2023000001',
'2023000002',
'2023000010',
'2023000011',
'2023000012',
'2023000022',
'2023000112',
'2023100000',
'2024000001',
'2024000002',
'2024000010',
'2024000011',
'2024000012',
'2024000022',
'2024000112',
'2025000001',
'2025000002',
'2025000003',
'2025000010',
'2025000011',
'2025000012',
'2025000013',
'2025000014',
'2025000020',
'2025000021',
'2025000022',
'2025000023',
'2025100000',
'2026000010',
'2026000110',
'2026000111',
'2026000112');

--========================================================================================================================

UPDATE mgr_risque03_016 SET octroi=ddbl WHERE TRIM(TO_CHAR(octroi)) IS NULL; 

---Modif 08/08/2017
INSERT INTO mgr_risque03_021 ( SELECT DISTINCT a.*,   substr(b.nid,1,17)  FROM mgr_risque03_016 a LEFT JOIN bkcli b ON (trim(a.cli)=trim(b.cli) and nid is not null and length(trim(nid))>0));
update mgr_risque03_021 set 
id_personne=( SELECT  substr(nidf,1,17)  FROM  bkcli b where  trim(mgr_risque03_021.cli)=trim(b.cli) and nidf is not null and length(trim(nidf))>0) 
where (trim(id_personne)='' or id_personne is null);

----�ch�ance
INSERT INTO mgr_risque03_007 SELECT a.age,   a.eve,   a.ave,   MAX(num) num  FROM mgr_risque03_016 a,bkechprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.eta='VA'  AND a.ctr=9
AND dva <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   GROUP BY a.age,   a.eve,   a.ave ;

---restant du et date de dernier paiement
INSERT INTO mgr_risque03_008 SELECT a.age,   a.eve,   a.ave,   a.num,   b.dva,   b.res  
FROM mgr_risque03_007 a LEFT JOIN bkechprt b ON a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.num=b.num  and ctr=9
AND dva <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY');

--premi�re echeance  a venir
INSERT INTO mgr_risque03_009 SELECT a.age,   a.eve,   a.ave,   a.num  FROM mgr_risque03_007 a,bkechprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.num=b.num  AND dva > to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY');

UPDATE mgr_risque03_009 SET num=num-1 WHERE 1=1;

INSERT INTO mgr_risque03_010 ( SELECT a.age,   a.eve,   a.ave,   a.num,   b.dva,   b.res  FROM mgr_risque03_009 a,bkechprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.num=b.num  );

INSERT INTO mgr_risque03_008 SELECT *  FROM mgr_risque03_010;

INSERT INTO mgr_risque03_023 ( SELECT a.*,   b.dva,   res,   '   ' code_fiche,   '1' qua_cred  
FROM mgr_risque03_021 a,mgr_risque03_008 b WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  );


INSERT INTO mgr_risque03_024 ( SELECT *  FROM mgr_risque03_021 WHERE age||eve NOT IN (SELECT age||eve  FROM mgr_risque03_023));

INSERT INTO mgr_risque03_025 ( SELECT a.age,   a.eve,   a.ave,   MAX(num) num  FROM mgr_risque03_024 a,bkechprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND b.eta='VA'  AND b.ctr != '0' and (num is not null)  GROUP BY a.age,   a.eve,   a.ave );

INSERT INTO mgr_risque03_026 ( SELECT a.age,   a.eve,   a.ave,   a.num,   b.dva,   res  FROM mgr_risque03_025 a,bkechprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.num=b.num  );
INSERT INTO mgr_risque03_023 SELECT a.*,   b.dva,   res,   '   ' code_fiche,   '1' qua_cred  FROM mgr_risque03_021 a,mgr_risque03_026 b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave;
INSERT INTO mgr_risque03_027 ( SELECT a.age,   a.eve,   a.ave  FROM mgr_risque03_024 a LEFT JOIN mgr_risque03_025 b ON  a.age||a.eve = b.age||b.eve 
WHERE TRIM(b.age||b.eve) IS NULL );
INSERT INTO mgr_risque03_028 ( SELECT a.age,   a.eve,   a.ave,   MIN(num) num  FROM mgr_risque03_027 a,bkechprt b WHERE a.age=b.age AND a.eve=b.eve  
AND a.ave=b.ave  AND b.eta='VA'  AND b.ctr='0' and (num is not null)  GROUP BY a.age,   a.eve,   a.ave );

INSERT INTO mgr_risque03_029 ( SELECT a.age,   a.eve,   a.ave,   a.num,   b.dva,   res  FROM mgr_risque03_028 a,bkechprt b WHERE a.age=b.age 
AND a.eve=b.eve  AND a.ave=b.ave  AND a.num=b.num  );

INSERT INTO mgr_risque03_023 SELECT a.*,   b.dva,   res,   '   ' code_fiche,   '1' qua_cred  FROM mgr_risque03_021 a,mgr_risque03_029 b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave;

UPDATE mgr_risque03_023 SET code_fiche=(SELECT code_fiche  FROM mgr_risque01_002 WHERE mgr_risque01_002.cha=mgr_risque03_023.cha_ori ) 
WHERE cha_ori IN (SELECT cha  FROM mgr_risque01_002);



INSERT INTO mgr_risque03_030 ( SELECT a.age,   a.eve,   a.ave,   MAX(num) num  FROM mgr_risque03_023 a,bkdblprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave   GROUP BY a.age,   a.eve,   a.ave );

INSERT INTO mgr_risque03_031 ( SELECT a.*,   b.dco  FROM mgr_risque03_030 a,bkdblprt b WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  
AND a.num=b.num  );

UPDATE mgr_risque03_023 SET ddbl=(SELECT dco  FROM mgr_risque03_031 WHERE mgr_risque03_031.age=mgr_risque03_023.age 
AND mgr_risque03_031.eve=mgr_risque03_023.eve  AND mgr_risque03_031.ave=mgr_risque03_023.ave ) 
WHERE age||eve IN (SELECT age||eve  FROM mgr_risque03_031);
INSERT INTO mgr_risque03_032 ( SELECT a.age,   a.eve,   a.ave,   SUM(b.mon) mon  FROM mgr_risque03_023 a,bkdblprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND dco > octroi  AND dco <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   
GROUP BY a.age,   a.eve,   a.ave );

UPDATE mgr_risque03_023 SET mdb=(SELECT mon  FROM mgr_risque03_032 WHERE mgr_risque03_023.age=mgr_risque03_032.age 
AND mgr_risque03_023.eve=mgr_risque03_032.eve  AND mgr_risque03_023.ave=mgr_risque03_032.ave ) 
WHERE age||eve IN (SELECT age||eve  FROM mgr_risque03_032);

UPDATE mgr_risque03_023 SET res=mdb WHERE res=0 AND dpec > to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY');

INSERT INTO mgr_risque03_033 ( SELECT a.age,   a.eve,   a.ave,   MAX(dco) dco_diff  FROM mgr_risque03_023 a,bkauxprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND sen='D'  AND lib LIKE '%DIFF%' 
AND dco <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   GROUP BY a.age,   a.eve,   a.ave );

INSERT INTO mgr_risque03_034 ( SELECT a.*,   SUM(mon) mon_diff  FROM mgr_risque03_033 a,bkauxprt b 
WHERE a.age=b.age AND a.eve=b.eve  AND a.ave=b.ave  AND a.dco_diff=b.dco  AND sen='D'  
AND lib LIKE '%DIFF%' AND dco <= to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')   GROUP BY a.age,   a.eve,   a.ave,   a.dco_diff );

INSERT INTO mgr_risque03_035 ( SELECT a.*,   (mdb+mon_diff) mdb_diff,   (mon+mon_diff) mon_octroi_diff,   (res+mon_diff) sde_eng_diff  
FROM mgr_risque03_023 a,mgr_risque03_034 b WHERE a.eve=b.eve AND a.age=b.age  AND a.ave=b.ave  );


INSERT INTO mgr_risque03_036 SELECT DISTINCT cli,   age,   ncpprt,   cha_ori,   '11',   '00008',   
to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'),   trim(id_personne),   trim(eve),   trim(code_fiche),   trim(devprt),   
octroi,   mon,   nbe,   amor,   unper,   ddbl,   mdb,   dpec,   res,   dva,   trim(qua_cred),   '0',   0,   0,   '0',   remb,   ' ',   
0,   0,   0,   0,   cum_int,   tau_int,   ind,   teg,   ' '  FROM mgr_risque03_023;

UPDATE mgr_risque03_036 SET sde_eng=(SELECT sde_eng_diff  FROM mgr_risque03_035 WHERE mgr_risque03_036.age=mgr_risque03_035.age 
AND mgr_risque03_036.ndos=mgr_risque03_035.eve ) WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_035) AND sde_eng=mdb;

UPDATE mgr_risque03_036 SET mon_octroi=(SELECT mon_octroi_diff  FROM mgr_risque03_035 WHERE mgr_risque03_036.age=mgr_risque03_035.age 
AND mgr_risque03_036.ndos=mgr_risque03_035.eve ),mdb=(SELECT mdb_diff  FROM mgr_risque03_035 WHERE mgr_risque03_036.age=mgr_risque03_035.age 
AND mgr_risque03_036.ndos=mgr_risque03_035.eve ) WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_035);

UPDATE mgr_risque03_036 SET imput=(SELECT sde  FROM mgr_risque03_015 WHERE mgr_risque03_015.age=mgr_risque03_036.age 
AND mgr_risque03_015.eve=mgr_risque03_036.ndos ) WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_015);

--Provision CDL
INSERT INTO mgr_risque03_037 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque03_011 a,bkcom b 
WHERE a.cli=b.cli AND cha like '29%'  AND cfe='N');

INSERT INTO mgr_risque03_038 ( SELECT a.*,   sde  FROM mgr_risque03_037 a,bksld b WHERE a.age=b.age AND a.ncp=b.ncp  
AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );

INSERT INTO mgr_risque03_039 ( SELECT DISTINCT a.age,   eve,   a.cli,   sde  FROM mgr_risque03_038 a,mgr_risque03_011 b WHERE a.cli=b.cli );

UPDATE mgr_risque03_036 SET monocd=(SELECT SUM(sde)  FROM mgr_risque03_039 WHERE mgr_risque03_036.age=mgr_risque03_039.age 
AND mgr_risque03_036.ndos=mgr_risque03_039.eve ) WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_039);

--Montant  CDL
delete from mgr_risque03_037 where 1=1;
delete from mgr_risque03_038 where 1=1;
delete from mgr_risque03_039 where 1=1;
INSERT INTO mgr_risque03_037 ( SELECT DISTINCT a.cli,   b.age,   b.dev,   b.ncp  FROM mgr_risque03_011 a,bkcom b 
WHERE a.cli=b.cli AND cha like '27%'  AND cfe='N');

INSERT INTO mgr_risque03_038 ( SELECT a.*,   sde  FROM mgr_risque03_037 a,bksld b WHERE a.age=b.age AND a.ncp=b.ncp  
AND dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')  );

INSERT INTO mgr_risque03_039 ( SELECT DISTINCT a.age,   eve,   a.cli,   sde  FROM mgr_risque03_038 a,mgr_risque03_011 b WHERE a.cli=b.cli );

UPDATE mgr_risque03_036 SET moncdl=(SELECT SUM(sde)  FROM mgr_risque03_039 WHERE mgr_risque03_036.age=mgr_risque03_039.age 
AND mgr_risque03_036.ndos=mgr_risque03_039.eve ) WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_039);
--Provision

UPDATE mgr_risque03_036 SET amor='F' WHERE amor='C';
UPDATE mgr_risque03_036 SET amor='D' WHERE amor='D';
UPDATE mgr_risque03_036 SET amor='P' WHERE amor='P';
UPDATE mgr_risque03_036 SET code_taux='TF' WHERE code_taux='F';
UPDATE mgr_risque03_036 SET code_taux='TV' WHERE code_taux='P';
UPDATE mgr_risque03_036 SET code_taux='TR' WHERE code_taux IN ('R','I');
UPDATE mgr_risque03_036 SET gar='1' WHERE age||ndos IN (SELECT DISTINCT age||eve  FROM mgr_risque03_020);
UPDATE mgr_risque03_036 SET mont=(SELECT SUM(mntg)  FROM mgr_risque03_020 WHERE mgr_risque03_020.age=mgr_risque03_036.age 
AND mgr_risque03_020.eve=mgr_risque03_036.ndos ) WHERE age||ndos IN (SELECT DISTINCT age||eve  FROM mgr_risque03_020);
UPDATE mgr_risque03_036 SET mont=0 WHERE gar='0';
UPDATE mgr_risque03_036 SET anti='1' WHERE age||ndos IN (SELECT age||eve  FROM mgr_risque03_012 WHERE remb != 0 );
UPDATE mgr_risque03_036 SET mon_anti=0 WHERE anti='0';
UPDATE mgr_risque03_036 SET sde_eng=abs(sde_eng) WHERE 1=1;
UPDATE mgr_risque03_036 SET unper='AU' WHERE unper NOT IN ('1','3','6','12');
UPDATE mgr_risque03_036 SET unper='ME' WHERE unper='1';
UPDATE mgr_risque03_036 SET unper='TR' WHERE unper='3';
UPDATE mgr_risque03_036 SET unper='SE' WHERE unper='6';
UPDATE mgr_risque03_036 SET unper='AN' WHERE unper='12';
update mgr_risque03_036 set nbe=999 where nbe>=1000;


UPDATE mgr_risque03_036 SET qua_cred=3 WHERE moncdl<>0;

insert into mgr_risque03_036_1
select CLI,AGE,NCP,CHA,OPE,ETAB,PER,ID_PERSONNE,NDOS,CODE,DEV,OCTROI,MON_OCTROI,substr(trim(to_char(NBE)),1,4),AMOR,UNPER,DDBL,MDB,DPEC,SDE_ENG,DDM_ENG,QUA_CRED,GAR,MONT,MONOCD,ANTI,MON_ANTI,0,
sde_eng,IMPUT,MONCDL,MONAGIO,PRODUIT,TAU_INT,CODE_TAUX,TEG,COMMENTAIRE,'',to_date('01/01/1900','DD/MM/YYYY'),0,0,0,0,'' from mgr_risque03_036;

update mgr_risque03_036_1 set interne='N' where 1=1;
update mgr_risque03_036_1 set interne='O' where cha like '207%';
update mgr_risque03_036_1 set ddm_eng='' where ddm_eng='01/01/1111';
update mgr_risque03_036_1 set dd_remb='' where dd_remb='01/01/1900';
update mgr_risque03_036_1 set dpec='' where dpec='01/01/1111';
update mgr_risque03_036_1 set gar='O'  where gar='1';
update mgr_risque03_036_1 set gar='N'  where gar='0';

update mgr_risque03_036_1 set per=substr(per,4,2)||'/'||substr(per,9,2) where 1=1;
update mgr_risque03_036_1 set octroi=ddbl where octroi is null;

update mgr_risque03_036_1 set moncdl=0 where vni30=0 and vni60=0 and vni90=0;
update mgr_risque03_036_1 set qua_cred=1 where vni30=0 and vni60=0 and vni90=0 and moncdl=0;
update mgr_risque03_036_1 set qua_cred=2 where vni30>0 or vni60>0 or vni90>0 or moncdl>0;

update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') where dd_remb in ('01/01/1111','01/01/1900');
update mgr_risque03_036_1 set dd_remb=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY') where dd_remb is null;

update mgr_risque03_036_1 set ddm_eng=(select ddc from bkcom where bkcom.age=substr(mgr_risque03_036_1.ndos,1,5) and bkcom.ncp=substr(mgr_risque03_036_1.ndos,6,11) and length(trim(ndos))=16) where (ddm_eng='') or (ddm_eng is null) or (ddm_eng in ('01/01/1111','01/01/1900'));
update mgr_risque03_036_1 set dpec=octroi where (dpec='') or (dpec is null) or (dpec in ('01/01/1111','01/01/1900'));


----------F06 DEVISES
update mgr_risque03_036_1 set encours=sde_eng*(select tind from bktau where bktau.dev=mgr_risque03_036_1.dev  and dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where dev in (select dev from bktau where dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'));
UPDATE mgr_risque03_036_1 SET dev=(SELECT substr(lib2,1,3)  FROM bknom WHERE ctab='005' AND mgr_risque03_036_1.dev=bknom.cacc ) WHERE dev IN (SELECT cacc  FROM bknom WHERE ctab='005' );
UPDATE mgr_risque03_036_1 SET dev='Ar' WHERE dev='MGA';

---MODIF 27/06/2019
--------------
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where 
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('01','03','05','07','08','10','12');
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'31' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');
update mgr_risque03_036_1 set ech_per='30'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)='31'  and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('04','06','09','11');

update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='29'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='30' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per=day(dpec)||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)<'29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');
update mgr_risque03_036_1 set ech_per='28'||'/0'||month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))||'/'||year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) where day(dpec)>='29' and
month(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY'))='02' and year(to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')) not in('2020','2024','2028','2032');



update mgr_risque03_036_1 set moncdl=encours where cha like '27%';
update mgr_risque03_036_1 set encours=0 where cha like '27%';
update mgr_risque03_036_1 set ddm_eng=ddbl where cha like '27%';

update mgr_risque03_036_1 set ddm_eng= ech_per where ddm_eng is null and cha not like ('27%');

update mgr_risque03_036_1 set encours=mdb where mdb-encours<0 and mdb-encours>-1;

update mgr_risque03_036_1 set qua_cred=1 where moncdl =0 and cha not like ('27%');
update mgr_risque03_036_1 set qua_cred=3,vni30=0 ,vni60=0 , vni90=0  where  cha  like ('27%');



update mgr_risque03_036_1 set commentaire='001' where dev='Ar';
update mgr_risque03_036_1 set commentaire='091' where dev='EUR';
update mgr_risque03_036_1 set commentaire='030' where dev='USD';
update mgr_risque03_036_1 set commentaire='069' where dev='GBP';
update mgr_risque03_036_1 set commentaire='074' where dev='CHF';
update mgr_risque03_036_1 set commentaire='103' where dev='CNY';
update mgr_risque03_036_1 set commentaire='049' where dev='ZAR';
update mgr_risque03_036_1 set commentaire='046' where dev='JPY';


update mgr_risque03_036_1 a set octroi=(select ddd from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
update mgr_risque03_036_1 a set octroi=(select dodb2 from bkcom b where substr(a.ndos,1,5)=b.age and substr(a.ndos,6,10)=substr(b.ncp,2,10) and 
substr(commentaire,1,3)=b.dev)
where exists (select 1 from bkcom where age||substr(ncp,2,10)=substr(a.ndos,1,15) and substr(a.commentaire,1,3)=dev) and octroi is null;
--select count(*) from mgr_risque03_03_3 where octroi is null;
update mgr_risque03_036_1 set ddbl=octroi where ddbl is null;
update mgr_risque03_036_1 set dpec=octroi where dpec is null;

-----------------


SET heading off
SET feedback off
SET pagesize 0
SET linesize 20000
SET trimspool on
SET colsep | 
SET echo off
SET numformat  9999999999999999990D9999

SPOOL /mgrxch/bnk/tmp/F05.txt
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is not null where  a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),' ',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F','{@DN}'||c.dna
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is not null where  a.dev='Ar'
union 
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi *b.tind,2)),to_char(round(mdb*b.tind,2)),to_char(round((mdb *b.tind)-encours-moncdl,2)),
to_char(round((encours),2)),to_char(round((sde_eng),2)),
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a join bktau b on  substr(a.commentaire,1,3)=b.dev and b.dco=to_char(last_day(add_months(MGR_GETDCO,-1)),'DD/MM/YYYY')
join  bkcli c on trim(a.cli)=trim(c.cli) and dna is null where  a.dev<>'Ar'
union
select  distinct a.etab,a.etab,a.per,ndos,ndos,a.id_personne,a.code,to_char(octroi,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(ddbl,'DD/MM/YYYY'),to_char(dpec,'DD/MM/YYYY'),
to_char(ddm_eng,'DD/MM/YYYY'),to_char(dd_remb,'DD/MM/YYYY'),ech_per,unper,
a.dev,interne,'N',to_char(round(mon_octroi,2)),to_char(round(mdb,2)),to_char(round(mdb-encours-moncdl,2)),
to_char(round((encours),2)),' ',
to_char(round(vni30,2)),to_char(round(vni60,2)),to_char(round(vni90,2)),to_char(round(moncdl,2)),to_char(round(nb_anti,2)),to_char(round(mon_anti,2)),'0',monocd,
to_char(round(monagio,2)),to_char(round(produit,2)),gar,qua_cred,to_char(round(tau_int,2)),code_taux,to_char(round(teg,2)),'','F',''
from mgr_risque03_036_1 a  join bkcli c on trim(a.cli)=trim(c.cli)  and dna is null where  a.dev='Ar';
SPOOL OFF;
SPOOL /mgrxch/bnk/tmp/F05_a_charger.txt
select * from bank.mgr_risque03_036_1;
SPOOL OFF;
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F05.txt /mgrxch/bnk/unl/F05.txt
!/applis/mgrp/bnk/bin/MGRUNLOAD /mgrxch/bnk/tmp/F05_a_charger.txt /mgrxch/bnk/unl/F05_a_charger.txt
